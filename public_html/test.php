<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11/3/15
 * Time: 4:01 PM
 */

// Пример получения отчета из GuardSaaS из внешней PHP программы

// Установите данные своего аккаунта для авторизации
//$user='files_84@mail.ru';
//$password = '888888889';
//
//$cookie_path = '/tmp/';
//$loginurl = 'https://app.guardsaas.com/login';
//$checkurl = 'https://app.guardsaas.com/login_check';
//$reporturl = 'https://app.guardsaas.com/reports/events/export';
//
//$ch = curl_init();
////get login form
//curl_setopt($ch, CURLOPT_URL, $loginurl);
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6');
//curl_setopt($ch, CURLOPT_TIMEOUT, 60);
//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_COOKIESESSION, true);
//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
//curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
//
//$response = curl_exec($ch);
//if (curl_errno($ch)) die(curl_error($ch));
//
//// Немного некрасивый парсинг токена... но работает.
//$pos = strpos($response,"_csrf_token");
//$token = substr($response,$pos);
//$token = substr($token,strpos($token,'value=')+6);
//$token = substr($token,1,strpos($token,'/>')-3);
//
////send post request to login_check with parameters (csrf_token included)
//curl_setopt($ch, CURLOPT_URL, $checkurl);
//curl_setopt($ch, CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
//    '_username' => $user,
//    '_password' => $password,
//    '_remember_me' => 'on', //if you use remember me functionality
//    '_csrf_token' => $token
//)));
//
//curl_exec($ch);
//if (curl_errno($ch)) die(curl_error($ch));
//
//// В конце концов выполняем свой запрос. Тут уже можено задавать параметры такие как надо
//$param = http_build_query(array(
//    'limit' => 100,
//    'offset' => 8,
//    'dateFrom' => '2019-11-14',
//    'dateTo' => '2019-11-18',
//    'order' => 'asc',
//));
//
//curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
////request login protected urls
//curl_setopt($ch, CURLOPT_URL, $reporturl);
//curl_setopt($ch, CURLOPT_POST, true);
//$report = curl_exec($ch);
//if (curl_errno($ch)) die(curl_error($ch));
//
//
//
//$report = curl_exec($ch);
//
//echo $report;
//curl_close($ch);
$ch = curl_init();

// {REPORT}
// events - Cобытия
// timesheet - Табель
// inout - Входы и Выходы
// discipline - Дисциплина
// statistics - Статистика
// attendance - Посещаемость
// movement - Перемещения

// {FORMAT}
// csv - текстовый, через запятую
// xls - Excel
// html - HTML
// xml - XML
// pdf - PDF

// {PARAMETERS}
// dateFrom=YYYY-MM-DD - дата начала отчета, месяц отчета
// dateTo=YYYY-MM-DD - дата конца отчета (если нужна)

$report = "events";
$format = "xls";
$param = http_build_query(array(
    'dateFrom' => '2019-11-14',
    'dateTo' => '2019-11-18',
    'appkey' => 's5anF2MuW9YRmVpYxG3GasCU6vdxbUp7',
));

curl_setopt($ch, CURLOPT_URL,"https://app.guardsaas.com/reports/export/$report/$format");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$param);

// In real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// Receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$reports = curl_exec($ch);
var_dump($reports);
exit;
?>