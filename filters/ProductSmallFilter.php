<?php

namespace app\filters;

use luya\admin\base\Filter;


class ProductSmallFilter extends Filter
{
    public static function identifier()
    {
        return 'product-small';
    }

    public function name()
    {
        return 'Маленькая картинка продукта 100х';
    }

    public function chain()
    {
        return [
            [self::EFFECT_THUMBNAIL, [
                'width' => 100,
                'height' => null,
            ]],
        ];
    }
}
