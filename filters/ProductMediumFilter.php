<?php
/**
 * Created by PhpStorm.
 * User: vlasov
 * Date: 25.08.2017
 * Time: 14:28
 */

namespace app\filters;

class ProductMediumFilter extends \luya\admin\base\Filter
{
    public static function identifier()
    {
        return 'product-medium';
    }

    public function name()
    {
        return 'картинка для продуктов 800x';
    }

    public function chain()
    {
        return [
            [self::EFFECT_THUMBNAIL, [
                'width' => 800,
                'height' => null,

            ]],
        ];
    }
}