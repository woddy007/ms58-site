<?php

namespace app\filters;

use luya\admin\base\Filter;


class ProductBigFilter extends Filter
{
    public static function identifier()
    {
        return 'product-full';
    }

    public function name()
    {
        return 'Большая картинка продукта 1200х';
    }

    public function chain()
    {
        return [
            [self::EFFECT_THUMBNAIL, [
                'width' => 1200,
                'height' => null,
            ]],
        ];
    }
}
