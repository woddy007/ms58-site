<?php
/**
 * Created by PhpStorm.
 * User: vlasov
 * Date: 25.08.2017
 * Time: 14:28
 */

namespace app\filters;

class ProductCatalogFilter extends \luya\admin\base\Filter
{
    public static function identifier()
    {
        return 'product-thumb';
    }

    public function name()
    {
        return 'картинка в каталоге продуктов 256x';
    }

    public function chain()
    {
        return [
            [self::EFFECT_THUMBNAIL, [
                'width' => 256,
                'height' => null,
            ]],
        ];
    }
}