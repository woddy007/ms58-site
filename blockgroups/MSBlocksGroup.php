<?php
namespace app\blockgroups;

class MSBlocksGroup extends \luya\cms\base\BlockGroup
{
    public function identifier()
    {
        return 'component-blocks-group';
    }

    public function label()
    {
        return 'Блоки MS58';
    }

    public function getPosition()
    {
        return 1;
    }
}
