<?php
/**
 * Created by PhpStorm.
 * User: vlasov
 * Date: 27.07.2017
 * Time: 22:03
 */


require 'vendor/luyadev/luya-deployer/luya.php';

set('shared_files', [
    'public_html/robots.txt',
    'public_html/.htaccess',

]);


task('replaces:files', function () {
    run('cp -r {{release_path}}/replaces/* {{release_path}}/vendor/');
});
after('deploy:symlink', 'replaces:files');

//set('beforeCommands', ['./vendor/bin/luya cms/updater/generic —interactive=0']);

// define your configuration here
server('prod', '91.201.52.104', 22)
    ->user('u13279')
    ->identityFile()
    ->stage('prod')
    ->env('deploy_path', '/home/u13279/ms58.su/source'); // Define the base path to deploy your project to.

set('repository', 'git@bitbucket.org:sitisit/lkn.git');
