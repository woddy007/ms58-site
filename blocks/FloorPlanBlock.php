<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Floor Plan Block.
 *
 * File has been created with `block/create` command.
 */
class FloorPlanBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'План этажа';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'title', 'label' => 'Заголовок', 'type' => self::TYPE_TEXT],
                 ['var' => 'plan', 'label' => 'План этажа', 'type' => self::TYPE_IMAGEUPLOAD, 'options' => ['no_filter' => false]],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        return [
            'plan' => BlockHelper::imageUpload($this->getVarValue('plan'), false, true),
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{extras.plan}}
     * @param {{vars.plan}}
     * @param {{vars.title}}
    */
    public function admin()
    {
        return '<h5 class="mb-3">Floor Plan Block</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.title is not empty %}' .
            '<tr><td><b>Заголовок</b></td><td>{{vars.title}}</td></tr>' .
            '{% endif %}'.
            '{% if vars.plan is not empty %}' .
            '<tr><td><b>План этажа</b></td><td>{{vars.plan}}</td></tr>' .
            '{% endif %}'.
            '</table>';
    }
}
