<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Block Mobile Info Block.
 *
 * File has been created with `block/create` command.
 */
class BlockMobileInfoBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Блок о мобильном приложении';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'title', 'label' => 'Заголовок', 'type' => self::TYPE_TEXT],
                 ['var' => 'link', 'label' => 'Ссылка', 'type' => self::TYPE_TEXT],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.link}}
     * @param {{vars.title}}
    */
    public function admin()
    {
        return '<h5 class="mb-3">Block Mobile Info Block</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.title is not empty %}' .
            '<tr><td><b>Заголовок</b></td><td>{{vars.title}}</td></tr>' .
            '{% endif %}'.
            '{% if vars.link is not empty %}' .
            '<tr><td><b>Ссылка</b></td><td>{{vars.link}}</td></tr>' .
            '{% endif %}'.
            '</table>';
    }
}
