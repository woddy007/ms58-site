<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * About Me Block.
 *
 * File has been created with `block/create` command.
 */
class AboutMeBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Информация о нас';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'title', 'label' => 'Заголовок', 'type' => self::TYPE_TEXT],
                 ['var' => 'description', 'label' => 'Описание', 'type' => 'zaa-ckeditor'],
                 ['var' => 'link', 'label' => 'Ссылка', 'type' => self::TYPE_TEXT],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.description}}
     * @param {{vars.link}}
     * @param {{vars.title}}
    */
    public function admin()
    {
        return '<h5 class="mb-3">Информация о нас</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.title is not empty %}' .
            '<tr><td><b>Заголовок</b></td><td>{{vars.title}}</td></tr>' .
            '{% endif %}'.
            '{% if vars.description is not empty %}' .
            '<tr><td><b>Описание</b></td><td>{{vars.description}}</td></tr>' .
            '{% endif %}'.
            '{% if vars.link is not empty %}' .
            '<tr><td><b>Ссылка</b></td><td>{{vars.link}}</td></tr>' .
            '{% endif %}'.
            '</table>';
    }
}
