<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Block Photo Gallery Block.
 *
 * File has been created with `block/create` command.
 */
class BlockPhotoGalleryBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Фотогалерея';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'title', 'label' => 'Заголовок', 'type' => self::TYPE_TEXT],
                 ['var' => 'images', 'label' => 'Изображения', 'type' => self::TYPE_IMAGEUPLOAD_ARRAY, 'options' => ['no_filter' => false]],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        return [
            'images' => BlockHelper::imageArrayUpload($this->getVarValue('images'), false, true),
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{extras.images}}
     * @param {{vars.images}}
     * @param {{vars.list}}
     * @param {{vars.title}}
    */
    public function admin()
    {
        return '<h5 class="mb-3">Block Photo Gallery Block</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.title is not empty %}' .
            '<tr><td><b>Заголовок</b></td><td>{{vars.title}}</td></tr>' .
            '{% endif %}'.
            '{% if vars.list is not empty %}' .
            '<tr><td><b>Изображения</b></td><td>{{vars.list}}</td></tr>' .
            '{% endif %}'.
            '{% if vars.images is not empty %}' .
            '<tr><td><b>Изображения</b></td><td>{{vars.images}}</td></tr>' .
            '{% endif %}'.
            '</table>';
    }
}
