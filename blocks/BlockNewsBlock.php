<?php

namespace app\blocks;

use app\modules\offices\models\Office;
use sitis\articles\core\entities\Article;
use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Block News Block.
 *
 * File has been created with `block/create` command.
 */
class BlockNewsBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Новости';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
        ];
    }

    public function extraVars()
    {
        return [
            'articles' => Article::find()->limit(3)->asArray()->all()
        ];
    }

    /**
     * {@inheritDoc}
     *
    */
    public function admin()
    {
        return '<h5 class="mb-3">Block News Block</h5>' .
            '<table class="table table-bordered">' .
            '</table>';
    }
}
