<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Banner Block.
 *
 * File has been created with `block/create` command.
 */
class BannerBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Банер';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'title', 'label' => 'Заголовок', 'type' => 'zaa-ckeditor'],
                ['var' => 'image', 'label' => 'Изображение', 'type' => self::TYPE_IMAGEUPLOAD],
                ['var' => 'list', 'label' => 'Список информации', 'type' => self::TYPE_MULTIPLE_INPUTS, 'options' => [
                    ['var' => 'icon', 'label' => 'Иконка', 'type' => self::TYPE_FILEUPLOAD],
                    ['var' => 'text', 'label' => 'Текст', 'type' => "zaa-ckeditor"],
                ],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.list}}
     * @param {{vars.title}}
     */
    public function admin()
    {
        return '<h5 class="mb-3">Banner Block</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.title is not empty %}' .
            '<tr><td><b>Заголовок</b></td><td>{{vars.title}}</td></tr>' .
            '{% endif %}' .
            '{% if vars.list is not empty %}' .
            '<tr><td><b>Список информации</b></td><td>{{vars.list}}</td></tr>' .
            '{% endif %}' .
            '</table>';
    }
}
