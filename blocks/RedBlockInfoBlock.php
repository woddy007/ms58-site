<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Red Block Info Block.
 *
 * File has been created with `block/create` command.
 */
class RedBlockInfoBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Красный блок с ссылкой';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                 ['var' => 'title', 'label' => 'Заголовок', 'type' => self::TYPE_TEXT],
                 ['var' => 'link', 'label' => 'Ссылка', 'type' => self::TYPE_TEXT],
                 ['var' => 'image', 'label' => 'Изображение фона', 'type' => self::TYPE_IMAGEUPLOAD],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.link}}
     * @param {{vars.title}}
    */
    public function admin()
    {
        return '<h5 class="mb-3">Красный блок с ссылкой</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.title is not empty %}' .
            '<tr><td><b>Заголовок</b></td><td>{{vars.title}}</td></tr>' .
            '{% endif %}'.
            '{% if vars.link is not empty %}' .
            '<tr><td><b>Ссылка</b></td><td>{{vars.link}}</td></tr>' .
            '{% endif %}'.
            '</table>';
    }
}
