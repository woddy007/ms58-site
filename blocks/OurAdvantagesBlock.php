<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Our Advantages Block.
 *
 * File has been created with `block/create` command.
 */
class OurAdvantagesBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Наши преимущества';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'title', 'label' => 'Заголовок', 'type' => self::TYPE_TEXT],
                ['var' => 'list', 'label' => 'Список преимуществ', 'type' => self::TYPE_MULTIPLE_INPUTS, 'options' => [
                    ['var' => 'image', 'label' => 'Изображение', 'type' => self::TYPE_IMAGEUPLOAD],
                    ['var' => 'laber', 'label' => 'Заголовок', 'type' => self::TYPE_TEXT],
                    ['var' => 'description', 'label' => 'Описание', 'type' => 'zaa-ckeditor'],
                ],
                ],
                ['var' => 'imageMap', 'label' => 'Изображение карты', 'type' => self::TYPE_IMAGEUPLOAD, 'options' => ['no_filter' => false]],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        return [
            'imageMap' => BlockHelper::imageUpload($this->getVarValue('imageMap'), false, true),
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{extras.imageMap}}
     * @param {{vars.imageMap}}
     * @param {{vars.list}}
     * @param {{vars.title}}
     */
    public function admin()
    {
        return '<h5 class="mb-3">Our Advantages Block</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.title is not empty %}' .
            '<tr><td><b>Заголовок</b></td><td>{{vars.title}}</td></tr>' .
            '{% endif %}' .
            '{% if vars.list is not empty %}' .
            '<tr><td><b>Список преимуществ</b></td><td>{{vars.list}}</td></tr>' .
            '{% endif %}' .
            '{% if vars.imageMap is not empty %}' .
            '<tr><td><b>Изображение карты</b></td><td>{{vars.imageMap}}</td></tr>' .
            '{% endif %}' .
            '</table>';
    }
}
