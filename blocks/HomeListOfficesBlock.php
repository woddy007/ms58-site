<?php

namespace app\blocks;

use app\modules\offices\models\Office;
use luya\cms\base\PhpBlock;
use app\blockgroups\MSBlocksGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Home List Offices Block.
 *
 * File has been created with `block/create` command.
 */
class HomeListOfficesBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return MSBlocksGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Список офисов на главной';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
        ];
    }

    public function extraVars()
    {
        return [
            'list' => Office::find()->limit(3)->asArray()->all()
        ];
    }

    /**
     * {@inheritDoc}
     *
    */
    public function admin()
    {
        return '<h5 class="mb-3">Home List Offices Block</h5>' .
            '<table class="table table-bordered">' .
            '</table>';
    }
}
