<?php

defined('YII_ENV') or define('YII_ENV', 'prod');
defined('YII_DEBUG') or define('YII_DEBUG', false);
$config = [
    'basePath' => dirname(__DIR__),
    'container' => [
        'definitions' => [
            \yii\filters\auth\HttpBearerAuth::class => \app\components\HttpBearerAuth::class,
        ],
    ],
    'modules' => [
        'admin' => [
            'jsonCruft' => false
        ],
        'articles' => [
            'class' => 'sitis\articles\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'articlesadmin' => 'sitis\articles\admin\Module',
        'styleguide' => [
            'class' => 'luya\styleguide\Module',
            'password' => '555',
            'assetFiles' => [
                'sitis\config\assets\WebpackAsset',
            ],
        ],
        'instructions' => [
            'class' => 'app\modules\instructions\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'instructionsadmin' => 'app\modules\instructions\admin\Module',
        
        'notification' => [
            'class' => 'app\modules\notification\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'notificationadmin' => 'app\modules\notification\admin\Module',
        'confroomreserv' => [
            'class' => 'app\modules\confroomreserv\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'confroomreservadmin' => 'app\modules\confroomreserv\admin\Module',
        'servicerequest' => [
            'class' => 'app\modules\servicerequest\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'servicerequestadmin' => 'app\modules\servicerequest\admin\Module',
        
        'offices' => [
            'class' => 'app\modules\offices\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'officesadmin' => 'app\modules\offices\admin\Module',
        'users' => [
            'class' => 'app\modules\users\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'usersadmin' => 'app\modules\users\admin\Module',
        'import' => [
            'class' => 'app\modules\import\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'importadmin' => 'app\modules\import\admin\Module',
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ms58',
            'username' => 'root',
            'password' => 'e7pm4jfeik',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 43200,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],
        'jwt' => [
            'class' => 'luya\admin\components\Jwt',
            'key' => 'MySecretJwtKey',
            'apiUserEmail' => '3@darkpiggy.xyz',
            'identityClass' => 'app\modules\users\models\Owner',
            'expireTime' => 3600 * 24 * 30
        ],
        'guardsaas' => [
            'class' => 'sitis\guardsaasreport\Guardsaasreportrequest',
            'user' => 'files_84@mail.ru',
            'password' => '888888889',
        ],

    ]
];

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/../vendor/sitis/config/common-config.php'), $config);