<?php

defined('YII_ENV') or define('YII_ENV', 'local');
defined('YII_DEBUG') or define('YII_DEBUG', true);

$config = [
    'basePath' => dirname(__DIR__),
    // 'bootstrap' => ['basicauth'],
    'components' => [
        'jwt' => [
            'class' => 'luya\admin\components\Jwt',
            'key' => 'MySecretJwtKey',
            'apiUserEmail' => '3@darkpiggy.xyz',
            'identityClass' => 'app\modules\users\models\Owner',
            'expireTime' => 3600 * 24 * 30
        ],
        'guardsaas' => [
            'class' => 'sitis\guardsaasreport\Guardsaasreportrequest',
            'user' => 'files_84@mail.ru',
            'password' => '888888889',
        ],
    ],
    'modules' => [
        // 'articles' => [
        //     'class' => 'sitis\articles\frontend\Module',
        //     'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        // ],
        // 'articlesadmin' => 'sitis\articles\admin\Module',
        'admin' => [
                    'class' => 'luya\admin\Module',
                    'secureLogin' => false, // when enabling secure login, the mail component must be proper configured otherwise the auth token mail will not send.
                    'strongPasswordPolicy' => false, // If enabled, the admin user passwords require strength input with special chars, lower, upper, digits and numbers
                    'interfaceLanguage' => 'ru', // Admin interface default language. Currently supported: en, de, ru, es, fr, ua, it, el, vi, pt, fa
                    'jsonCruft' => false
                ],
        'styleguide' => [
            'class' => 'luya\styleguide\Module',
            'password' => '555',
            'assetFiles' => [
                'sitis\config\assets\WebpackAsset',
            ],
        ],
        // 'bills' => [
        //     'class' => 'app\modules\bills\frontend\Module',
        //     'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        // ],
        // 'billsadmin' => 'app\modules\bills\admin\Module',
        'instructions' => [
            'class' => 'app\modules\instructions\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'instructionsadmin' => 'app\modules\instructions\admin\Module',
        
        'notification' => [
            'class' => 'app\modules\notification\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'notificationadmin' => 'app\modules\notification\admin\Module',
        'confroomreserv' => [
            'class' => 'app\modules\confroomreserv\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'confroomreservadmin' => 'app\modules\confroomreserv\admin\Module',
        'servicerequest' => [
            'class' => 'app\modules\servicerequest\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'servicerequestadmin' => 'app\modules\servicerequest\admin\Module',
        
        'offices' => [
            'class' => 'app\modules\offices\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'officesadmin' => 'app\modules\offices\admin\Module',
        'users' => [
            'class' => 'app\modules\users\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'usersadmin' => 'app\modules\users\admin\Module',
        'import' => [
            'class' => 'app\modules\import\frontend\Module',
            'useAppViewPath' => true, // When enabled the views will be looked up in the @app/views folder, otherwise the views shipped with the module will be used.
        ],
        'importadmin' => 'app\modules\import\admin\Module',
    ]
];

if (YII_DEBUG && isset($_SERVER['REQUEST_URI']) && false === strpos($_SERVER['REQUEST_URI'], 'admin')) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['14.0.0.*', '127.0.0.1'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['14.0.0.*', '127.0.0.1'],
    ];
}

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/../vendor/sitis/config/common-config.php'), require('env-local-db.php'), $config);