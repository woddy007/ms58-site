<?php

return [
    'heading1' => [function($foo) {
        return '<h1>'.$foo.'</h1>';
    }, [
        'foo' => 'Заголовок 1',
    ]],
    'heading2' => [function($foo) {
        return '<h2>'.$foo.'</h2>';
    }, [
        'foo' => 'Заголовок 2',
    ]],
    'heading3' => [function($foo) {
        return '<h3>'.$foo.'</h3>';
    }, [
        'foo' => 'Заголовок 3',
    ]],
    'heading4' => [function($foo) {
        return '<h4>'.$foo.'</h4>';
    }, [
        'foo' => 'Заголовок 4',
    ]],
    'heading5' => [function($foo) {
        return '<h5>'.$foo.'</h5>';
    }, [
        'foo' => 'Заголовок 5',
    ]],
    'heading6' => [function($foo) {
        return '<h6>'.$foo.'</h6>';
    }, [
        'foo' => 'Заголовок 6',
    ]],
    'paragraph' => [function($foo) {
        return '<p>'.$foo.'</p>';
    }, [
        'foo' => 'Абзац',
    ]],
    'ul' => [function($foo) {
        return '<ul><li>'.$foo.'</li><li>'.$foo.'</li><li>'.$foo.'</li></ul>';
    }, [
        'foo' => 'Маркированный список',
    ]],
    'ol' => [function($foo) {
        return '<ol><li>'.$foo.'</li><li>'.$foo.'</li><li>'.$foo.'</li></ol>';
    }, [
        'foo' => 'Нумерованный список',
    ]],
];

?>

