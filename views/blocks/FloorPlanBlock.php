<?php
/**
 * View file for block: FloorPlanBlock
 *
 * File has been created with `block/create` command.
 *
 * @param $this->extraValue('plan');
 * @param $this->varValue('plan');
 * @param $this->varValue('title');
 *
 * @var \luya\cms\base\PhpBlockView $this
 */

use sitis\blocks\components\helpers\ImageHelper as ImageHelperAlias;

?>

<div class="tm-floor-plan">
    <div class="uk-container">
        <div class="tm-floor-plan__content">
            <div class="tm-title tm-floor-plan__title"><?= $this->varValue('title') ?></div>
            <div class="tm-floor-plan__image-content">
                <img src="<?= ImageHelperAlias::getImage($this->varValue('plan')) ?>" class="tm-floor-plan__image">
            </div>
        </div>
    </div>
</div>
