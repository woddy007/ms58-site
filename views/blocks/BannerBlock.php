<?php
/**
 * View file for block: BannerBlock
 *
 * File has been created with `block/create` command.
 *
 * @param $this ->varValue('list');
 * @param $this ->varValue('title');
 * @param $this ->varValue('image');
 *
 * @var \luya\cms\base\PhpBlockView $this
 */

use sitis\blocks\components\helpers\ImageHelper;

?>

<div class="tm-banner">
    <div class="uk-container">
        <div class="tm-banner__content">
            <div class="tm-banner__title">
                <?= $this->varValue('title') ?>
            </div>
            <div class="tm-banner__items">
                <?php foreach ($this->varValue('list') as $item): ?>
                    <div class="tm-banner__item">
                        <img src="<?= Yii::$app->storage->getFile($item['icon'])->source ?>" class="tm-banner__item-image">
                        <div class="tm-banner__item-label"><?= $item['text'] ?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <img
            src="<?= ImageHelper::getImage($this->varValue('image')) ?>"
            class="tm-banner__background-image"
    >
</div>
