<?php
/**
 * View file for block: AboutMeBlock
 *
 * File has been created with `block/create` command.
 *
 * @param $this->varValue('description');
 * @param $this->varValue('link');
 * @param $this->varValue('title');
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<div class="tm-about-block">
    <div class="uk-container">
        <div class="tm-about-block__content">
            <img src="/static/app/icons/big-logo.png" class="tm-about-block__logo">
            <div class="tm-about-block__info">
                <div class="tm-about-block__title"><?= $this->varValue('title') ?></div>
                <div class="tm-about-block__description"><?= $this->varValue('description') ?></div>
                <a href="<?= $this->varValue('link') ?>" class="uk-button uk-button-link">
                    ПОДРОБНЕЕ
                </a>
            </div>
        </div>
    </div>
</div>
