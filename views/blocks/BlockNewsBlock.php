<?php
/**
 * View file for block: BlockNewsBlock
 *
 * File has been created with `block/create` command.
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<div class="tm-block-news">
    <div class="uk-container">
        <div class="tm-block-news__top">
            <div class="tm-title tm-block-news__title">Новости</div>
            <a href="#" class="uk-button uk-button-link">ВСЕ НОВОСТИ</a>
        </div>

        <div class="tm-block-news__link-list">
            <a href="#" class="tm-card-news">
                <div class="tm-card-news__label">Сбербанк снизил цену на один из крупных офисов</div>
                <div class="tm-card-news__description">
                    Также продается земельный участок общей площадью 1696 кв.м., складское помещение
                </div>
                <img
                    src="https://s3-alpha-sig.figma.com/img/e7cd/e8ab/065c41bd35d66a8a09890cafd6cae050?Expires=1578268800&Signature=GWDSRp8QbIq27132sRB3ilzGDmGaWb5rCrKIhRnbfryc3nQbjkE-AUCtC3IOWebjjJXLvBff1WGFf4Cl-RzZd8CpJqYlUY1OfNqZPBP16wWfCSE48qzv38j3HZUU1wpgDTuLxP2O6-BR0IE1MqYxe~2s1SnPiBrrZlQ9B8V7kj7pYAh-YuQ2YjYBFPmO5IvlwYLHHIRgJTlywL6hv86mnDcUcEG7ke7j874Buj2A8ma6Z-rjBHIUsj63T50bx6iGJikyEmpdVomWB6VjJF83KTMWHHG1cVF7opBQpn-bL2IuP6g2FfwSYLZzBdNVt9xzhALuIhUkADQKdgq896vUFQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
                    class="tm-card-news__image"
                >
            </a>
            <a href="#" class="tm-card-news">
                <div class="tm-card-news__label">Сбербанк снизил цену на один из крупных офисов</div>
                <div class="tm-card-news__description">
                    Также продается земельный участок общей площадью 1696 кв.м., складское помещение
                </div>
                <img
                    src="https://s3-alpha-sig.figma.com/img/e7cd/e8ab/065c41bd35d66a8a09890cafd6cae050?Expires=1578268800&Signature=GWDSRp8QbIq27132sRB3ilzGDmGaWb5rCrKIhRnbfryc3nQbjkE-AUCtC3IOWebjjJXLvBff1WGFf4Cl-RzZd8CpJqYlUY1OfNqZPBP16wWfCSE48qzv38j3HZUU1wpgDTuLxP2O6-BR0IE1MqYxe~2s1SnPiBrrZlQ9B8V7kj7pYAh-YuQ2YjYBFPmO5IvlwYLHHIRgJTlywL6hv86mnDcUcEG7ke7j874Buj2A8ma6Z-rjBHIUsj63T50bx6iGJikyEmpdVomWB6VjJF83KTMWHHG1cVF7opBQpn-bL2IuP6g2FfwSYLZzBdNVt9xzhALuIhUkADQKdgq896vUFQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
                    class="tm-card-news__image"
                >
            </a>
            <a href="#" class="tm-card-news">
                <div class="tm-card-news__label">Сбербанк снизил цену на один из крупных офисов</div>
                <div class="tm-card-news__description">
                    Также продается земельный участок общей площадью 1696 кв.м., складское помещение
                </div>
                <img
                    src="https://s3-alpha-sig.figma.com/img/e7cd/e8ab/065c41bd35d66a8a09890cafd6cae050?Expires=1578268800&Signature=GWDSRp8QbIq27132sRB3ilzGDmGaWb5rCrKIhRnbfryc3nQbjkE-AUCtC3IOWebjjJXLvBff1WGFf4Cl-RzZd8CpJqYlUY1OfNqZPBP16wWfCSE48qzv38j3HZUU1wpgDTuLxP2O6-BR0IE1MqYxe~2s1SnPiBrrZlQ9B8V7kj7pYAh-YuQ2YjYBFPmO5IvlwYLHHIRgJTlywL6hv86mnDcUcEG7ke7j874Buj2A8ma6Z-rjBHIUsj63T50bx6iGJikyEmpdVomWB6VjJF83KTMWHHG1cVF7opBQpn-bL2IuP6g2FfwSYLZzBdNVt9xzhALuIhUkADQKdgq896vUFQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
                    class="tm-card-news__image"
                >
            </a>
        </div>
    </div>
</div>
