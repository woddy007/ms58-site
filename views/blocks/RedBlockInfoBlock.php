<?php
/**
 * View file for block: RedBlockInfoBlock
 *
 * File has been created with `block/create` command.
 *
 * @param $this->varValue('link');
 * @param $this->varValue('title');
 * @param $this->varValue('image');
 *
 * @var \luya\cms\base\PhpBlockView $this
 */

use sitis\blocks\components\helpers\ImageHelper as ImageHelperAlias; ?>

<div class="tm-info-block">
    <div class="uk-container">
        <div class="tm-info-block__content">
            <div class="tm-info-block__title"><?= $this->varValue('title') ?></div>
            <button class="uk-button uk-button-secondary">Заказать</button>
        </div>
    </div>

    <img src="<?= ImageHelperAlias::getImage( $this->varValue('image') ) ?>" class="tm-info-block__image-background">
</div>
