<?php
/**
 * View file for block: BlockPhotoGalleryBlock
 *
 * File has been created with `block/create` command.
 *
 * @param $this ->extraValue('images');
 * @param $this ->varValue('images');
 * @param $this->varValue('title');
 *
 * @var \luya\cms\base\PhpBlockView $this
 */

use sitis\blocks\components\helpers\ImageHelper as ImageHelperAlias;

?>

<div class="tm-b-photo-gallery">
    <div class="uk-container">
        <div class="tm-title tm-b-photo-gallery__title"><?= $this->varValue('title') ?></div>
        <div class="tm-b-photo-gallery__list" data-uk-lightbox="toggle: > div">
            <?php foreach ($this->varValue('images') as $item):?>
                <div href="<?= ImageHelperAlias::getImage($item['imageId']) ?>" class="tm-b-photo-gallery__item" data-alt="Image" data-type="image">
                    <img src="<?= ImageHelperAlias::getImage($item['imageId']) ?>">
                </div>
            <?php endforeach; ?>
            <a href="#asd" class="tm-b-photo-gallery__item tm-b-photo-gallery__all-photo" data-type="iframe">
                Все фото
            </a>
        </div>
    </div>
</div>
