<?php
/**
 * View file for block: OurAdvantagesBlock
 *
 * File has been created with `block/create` command.
 *
 * @param $this ->extraValue('imageMap');
 * @param $this->varValue('imageMap');
 * @param $this ->varValue('list');
 * @param $this ->varValue('title');
 *
 * @var \luya\cms\base\PhpBlockView $this
 */

use sitis\blocks\components\helpers\ImageHelper;

?>

<div class="tm-our-advantage">
    <div class="uk-container">
        <div class="tm-our-advantage__content">
            <div class="tm-title tm-our-advantage__title"><?= $this->varValue('title') ?></div>
            <div class="tm-our-advantage__list">
                <?php foreach ($this->varValue('list') as $item): ?>
                    <a href="#" class="tm-our-advantage__item">
                        <img src="<?= ImageHelper::getImage($item['image']) ?>" class="tm-our-advantage__item-image">
                        <div class="tm-our-advantage__item-label"><?= $item['laber'] ?></div>
                        <div class="tm-our-advantage__item-description"><?= $item['description'] ?></div>
                    </a>
                <?php endforeach; ?>
            </div>
            <div class="tm-our-advantage__map">
                <img src="<?= ImageHelper::getImage($this->varValue('imageMap')) ?>" class="tm-our-advantage__map-image">
            </div>
        </div>
    </div>
</div>
