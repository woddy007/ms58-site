<?php
/**
 * View file for block: BlockMobileInfoBlock
 *
 * File has been created with `block/create` command.
 *
 * @param $this->varValue('link');
 * @param $this->varValue('title');
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<div class="tm-mobile-app">
    <div class="uk-container">
        <div class="tm-mobile-app__content">
            <div class="tm-mobile-app__content-left">
                <div class="tm-mobile-app__title">
                    <?= $this->varValue('title') ?>
                </div>
                <a href="<?= $this->varValue('link') ?>" class="uk-button uk-button-secondary tm-mobile-app__link">
                    Перейти
                </a>
            </div>
            <div class="tm-mobile-app__content-right">
                <img src="/static/app/images/phone-app-image.png" class="tm-mobile-app__image">
            </div>
        </div>
    </div>
</div>
