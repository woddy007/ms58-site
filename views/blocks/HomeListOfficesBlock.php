<?php
/**
 * View file for block: HomeListOfficesBlock
 *
 * File has been created with `block/create` command.
 *
 *
 * @param $this->extraValue('list');
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<div class="tm-list-office">
    <div class="uk-container">
        <div class="tm-list-office__content">
            <div class="tm-list-office__list">
                <?php foreach ($this->extraValue('list') as $key=>$item): ?>
                    <div class="tm-big-card-office <?= ( $key % 2 == 0 )? '': 'tm-big-card-office--right' ?>">
                        <div class="tm-big-card-office__left">
                            <img
                                    src="https://s3-alpha-sig.figma.com/img/a54e/3c24/5dadd633cdfa2ffa7eec0e2bc2572861?Expires=1578268800&Signature=XfpXnT7NPvMJrMlsBVzfaC~9JZYX2B52-5M~Sj5Vvv2ai22YrkCACYu~BV5y8HQClVzQdAKplGnP902lmFoRxbLWJu-wsvGp7JNTYNZt4q~zF2ej3kzHZIb8m5H8tNuenWs29AbmBtR4NBjHZ2PsXBpLenQm91kQe6VCAyWidlXpAxDcQmvin9EJXnwqCOn80pmy8ZxqLlTdHzP0IypMH99NETCGHRBZug1TQGG14CmVRmEgW6SObK60-p5JBbtMdum8jbpAnUEwgC5pJXhiO5NcHmdlVDGzEW2mZ-IRF1OAc8u7Hfgy611QI9xEdmhggosAwK0N0Mw0dTtNFOLqPA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
                                    class="tm-big-card-office__image"
                            >

                            <a href="#" class="tm-big-card-office__arrow">
                                <img src="/static/app/icons/arrow-right.svg">
                            </a>
                        </div>
                        <div class="tm-big-card-office__right">
                            <div class="tm-big-card-office__title">Офис 10.02</div>
                            <ul class="tm-big-card-office_info-list">
                                <li class="tm-big-card-office_info-item">
                                    Площадь: <span>64.5 м2</span>
                                </li>
                                <li class="tm-big-card-office_info-item">
                                    Этаж: <span>10</span>
                                </li>
                                <li class="tm-big-card-office_info-item">
                                    Аренда: <span>20 000 ₽/мес</span>
                                </li>
                                <li class="tm-big-card-office_info-item">
                                    Видеонаблюдение
                                </li>
                                <li class="tm-big-card-office_info-item">
                                    Новый ремонт
                                </li>
                            </ul>
                            <button class="uk-button uk-button-primary">Посмотреть</button>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <button class="uk-button uk-button-secondary tm-list-office__button-all">
                Все объекты
            </button>
        </div>
    </div>
</div>
