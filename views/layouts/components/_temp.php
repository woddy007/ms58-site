<section class="imagebg image--light cover cover-blocks bg--secondary ">
    <div class="background-image-holder hidden-xs">
        <img alt="background" src="img/promo-1.jpg"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-5 ">
                <div>
                    <h1>Features Small</h1>
                    <p class="lead">
                        Use any of these customisable blocks in your designs. The tabs below show just some of the
                        possible combinations.
                    </p>
                    <hr class="short">
                    <p>
                        All blocks shown here are available to use in
                        <br class="hidden-xs hidden-sm"/> the included
                        <a href="#" target="_blank">Variant Page Builder &nearr;</a>
                    </p>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 1
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Built for teams</h5>
                                    <p>
                                        Ideal for large and small organisations
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Modern Aesthetic</h5>
                                    <p>
                                        A highly adaptable look that's simple
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature--featured feature-1 boxed boxed--border">
                                    <h5>Beautiful markup</h5>
                                    <p>
                                        Following BEM conventions for readability
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">Hot</span>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Built for teams</h5>
                                    <p>
                                        Ideal for large and small organisations
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Modern Aesthetic</h5>
                                    <p>
                                        A highly adaptable look that's simple
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature--featured feature-1 boxed boxed--border">
                                    <h5>Beautiful markup</h5>
                                    <p>
                                        Following BEM conventions for readability
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">Hot</span>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Built for teams</h5>
                                    <p>
                                        Ideal for large and small organisations
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Modern Aesthetic</h5>
                                    <p>
                                        A highly adaptable look that's simple
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature--featured feature-1 boxed boxed--border">
                                    <h5>Beautiful markup</h5>
                                    <p>
                                        Following BEM conventions for readability
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">Hot</span>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class=" imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Built for teams</h5>
                                    <p>
                                        Ideal for large and small organisations
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1 boxed boxed--border">
                                    <h5>Modern Aesthetic</h5>
                                    <p>
                                        A highly adaptable look that's simple
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature--featured feature-1 boxed boxed--border">
                                    <h5>Beautiful markup</h5>
                                    <p>
                                        Following BEM conventions for readability
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">Hot</span>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 2
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Clock-Back color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Duplicate-Window color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Construct mockups or production-ready pages in-browser with Variant Page
                                            Builder
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Life-Jacket color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Take comfort in 6 months included support with a dedicated support forum
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Clock-Back color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Duplicate-Window color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Construct mockups or production-ready pages in-browser with Variant Page
                                            Builder
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Life-Jacket color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Take comfort in 6 months included support with a dedicated support forum
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Clock-Back color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Duplicate-Window color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Construct mockups or production-ready pages in-browser with Variant Page
                                            Builder
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Life-Jacket color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Take comfort in 6 months included support with a dedicated support forum
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class=" imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Clock-Back color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Duplicate-Window color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Construct mockups or production-ready pages in-browser with Variant Page
                                            Builder
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-2 boxed boxed--border">
                                    <i class="icon icon-Life-Jacket color--primary"></i>
                                    <div class="feature__body">
                                        <p>
                                            Take comfort in 6 months included support with a dedicated support forum
                                        </p>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 3
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class="text-center ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Mail-3"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for modern marketing campaigns
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Air-Balloon"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        Including the premium Icons Mind icon kit, Stack features a highly diverse set
                                        of icons suitable for all purposes.
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">New</span>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Bacteria"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        Combine blocks from a range of categories to build pages that are rich in visual
                                        style and interactivity
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Mail-3"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for modern marketing campaigns
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Air-Balloon"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        Including the premium Icons Mind icon kit, Stack features a highly diverse set
                                        of icons suitable for all purposes.
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">New</span>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Bacteria"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        Combine blocks from a range of categories to build pages that are rich in visual
                                        style and interactivity
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Mail-3"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for modern marketing campaigns
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Air-Balloon"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        Including the premium Icons Mind icon kit, Stack features a highly diverse set
                                        of icons suitable for all purposes.
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">New</span>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Bacteria"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        Combine blocks from a range of categories to build pages that are rich in visual
                                        style and interactivity
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class="text-center imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Mail-3"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for modern marketing campaigns
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Air-Balloon"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        Including the premium Icons Mind icon kit, Stack features a highly diverse set
                                        of icons suitable for all purposes.
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                    <span class="label">New</span>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-3 boxed boxed--lg boxed--border">
                                    <i class="icon icon--lg icon-Bacteria"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        Combine blocks from a range of categories to build pages that are rich in visual
                                        style and interactivity
                                    </p>
                                    <a href="#">
                                        Learn More
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 4
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hand-Touch"></i>
                                    <h4>Interactive Elements</h4>
                                    <hr>
                                    <p>
                                        With rich modal and notification functionality and a robust suite of options,
                                        Stack makes building feature-heavy pages simple and enjoyable.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hour"></i>
                                    <h4>Save Development Time</h4>
                                    <hr>
                                    <p>
                                        Drastically reduce the time it takes to move from initial concept to
                                        production-ready with Stack and Variant Page Builder. Your clients will love you
                                        for it!
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Life-Safer"></i>
                                    <h4>Friendly Support</h4>
                                    <hr>
                                    <p>
                                        Our customers love the comfort that comes with six-months free support. Our
                                        dedicated support forum makes interacting with us hassle-free and efficient.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hand-Touch"></i>
                                    <h4>Interactive Elements</h4>
                                    <hr>
                                    <p>
                                        With rich modal and notification functionality and a robust suite of options,
                                        Stack makes building feature-heavy pages simple and enjoyable.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hour"></i>
                                    <h4>Save Development Time</h4>
                                    <hr>
                                    <p>
                                        Drastically reduce the time it takes to move from initial concept to
                                        production-ready with Stack and Variant Page Builder. Your clients will love you
                                        for it!
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Life-Safer"></i>
                                    <h4>Friendly Support</h4>
                                    <hr>
                                    <p>
                                        Our customers love the comfort that comes with six-months free support. Our
                                        dedicated support forum makes interacting with us hassle-free and efficient.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hand-Touch"></i>
                                    <h4>Interactive Elements</h4>
                                    <hr>
                                    <p>
                                        With rich modal and notification functionality and a robust suite of options,
                                        Stack makes building feature-heavy pages simple and enjoyable.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hour"></i>
                                    <h4>Save Development Time</h4>
                                    <hr>
                                    <p>
                                        Drastically reduce the time it takes to move from initial concept to
                                        production-ready with Stack and Variant Page Builder. Your clients will love you
                                        for it!
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Life-Safer"></i>
                                    <h4>Friendly Support</h4>
                                    <hr>
                                    <p>
                                        Our customers love the comfort that comes with six-months free support. Our
                                        dedicated support forum makes interacting with us hassle-free and efficient.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class=" imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hand-Touch"></i>
                                    <h4>Interactive Elements</h4>
                                    <hr>
                                    <p>
                                        With rich modal and notification functionality and a robust suite of options,
                                        Stack makes building feature-heavy pages simple and enjoyable.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Hour"></i>
                                    <h4>Save Development Time</h4>
                                    <hr>
                                    <p>
                                        Drastically reduce the time it takes to move from initial concept to
                                        production-ready with Stack and Variant Page Builder. Your clients will love you
                                        for it!
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-4 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Life-Safer"></i>
                                    <h4>Friendly Support</h4>
                                    <hr>
                                    <p>
                                        Our customers love the comfort that comes with six-months free support. Our
                                        dedicated support forum makes interacting with us hassle-free and efficient.
                                    </p>
                                    <a class="btn btn--primary" href="#">
                                                    <span class="btn__text">
                                                        Learn More
                                                    </span>
                                    </a>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 5
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Pantone icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Highly Customizable</h5>
                                        <p>
                                            Stack's visual style is simple yet distinct perfect for any project whether
                                            it be a basic marketing site, or multi-page company presence.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Fingerprint icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Built For Startups</h5>
                                        <p>
                                            Launching an attractive website quickly and affordably is important for
                                            modern startups &mdash; Stack offers massive value with modern styling.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Pantone icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Highly Customizable</h5>
                                        <p>
                                            Stack's visual style is simple yet distinct perfect for any project whether
                                            it be a basic marketing site, or multi-page company presence.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Fingerprint icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Built For Startups</h5>
                                        <p>
                                            Launching an attractive website quickly and affordably is important for
                                            modern startups &mdash; Stack offers massive value with modern styling.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Pantone icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Highly Customizable</h5>
                                        <p>
                                            Stack's visual style is simple yet distinct perfect for any project whether
                                            it be a basic marketing site, or multi-page company presence.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Fingerprint icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Built For Startups</h5>
                                        <p>
                                            Launching an attractive website quickly and affordably is important for
                                            modern startups &mdash; Stack offers massive value with modern styling.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class=" imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Pantone icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Highly Customizable</h5>
                                        <p>
                                            Stack's visual style is simple yet distinct perfect for any project whether
                                            it be a basic marketing site, or multi-page company presence.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                                    <i class="icon icon-Fingerprint icon--lg"></i>
                                    <div class="feature__body">
                                        <h5>Built For Startups</h5>
                                        <p>
                                            Launching an attractive website quickly and affordably is important for
                                            modern startups &mdash; Stack offers massive value with modern styling.
                                        </p>
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 6
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Post-Mail2 color--primary"></i>
                                    <h5>Collect Leads</h5>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for launching modern marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Preview color--primary"></i>
                                    <h5>Showcase Products</h5>
                                    <p>
                                        Give your products the presentation they deserve with Stack's uniquely presents
                                        e-commerce styling &mdash; perfectly suited to tech products
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Pen color--primary"></i>
                                    <h5>Engage Users</h5>
                                    <p>
                                        Taking inspiration from high-profile tech companies, Stack's diverse blog styles
                                        make your written-content shine and your user's smile
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Road-3 color--primary"></i>
                                    <h5>Launch Quickly</h5>
                                    <p>
                                        Forget handling cumbersome mockups. Get building immediately in the comfort of
                                        your browser. Our customers love the ability to launch beautiful sites quickly.
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Post-Mail2 color--primary"></i>
                                    <h5>Collect Leads</h5>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for launching modern marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Preview color--primary"></i>
                                    <h5>Showcase Products</h5>
                                    <p>
                                        Give your products the presentation they deserve with Stack's uniquely presents
                                        e-commerce styling &mdash; perfectly suited to tech products
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Pen color--primary"></i>
                                    <h5>Engage Users</h5>
                                    <p>
                                        Taking inspiration from high-profile tech companies, Stack's diverse blog styles
                                        make your written-content shine and your user's smile
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Road-3 color--primary"></i>
                                    <h5>Launch Quickly</h5>
                                    <p>
                                        Forget handling cumbersome mockups. Get building immediately in the comfort of
                                        your browser. Our customers love the ability to launch beautiful sites quickly.
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Post-Mail2 color--primary"></i>
                                    <h5>Collect Leads</h5>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for launching modern marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Preview color--primary"></i>
                                    <h5>Showcase Products</h5>
                                    <p>
                                        Give your products the presentation they deserve with Stack's uniquely presents
                                        e-commerce styling &mdash; perfectly suited to tech products
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Pen color--primary"></i>
                                    <h5>Engage Users</h5>
                                    <p>
                                        Taking inspiration from high-profile tech companies, Stack's diverse blog styles
                                        make your written-content shine and your user's smile
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Road-3 color--primary"></i>
                                    <h5>Launch Quickly</h5>
                                    <p>
                                        Forget handling cumbersome mockups. Get building immediately in the comfort of
                                        your browser. Our customers love the ability to launch beautiful sites quickly.
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class=" imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Post-Mail2 color--primary"></i>
                                    <h5>Collect Leads</h5>
                                    <p>
                                        Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal
                                        for launching modern marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Preview color--primary"></i>
                                    <h5>Showcase Products</h5>
                                    <p>
                                        Give your products the presentation they deserve with Stack's uniquely presents
                                        e-commerce styling &mdash; perfectly suited to tech products
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Pen color--primary"></i>
                                    <h5>Engage Users</h5>
                                    <p>
                                        Taking inspiration from high-profile tech companies, Stack's diverse blog styles
                                        make your written-content shine and your user's smile
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="feature feature-6">
                                    <i class="icon icon--sm icon-Road-3 color--primary"></i>
                                    <h5>Launch Quickly</h5>
                                    <p>
                                        Forget handling cumbersome mockups. Get building immediately in the comfort of
                                        your browser. Our customers love the ability to launch beautiful sites quickly.
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 7
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/blog-1.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Built for teams</h5>
                                        <p>
                                            Ideal for large and small organisations
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-5.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Modern Aesthetic</h5>
                                        <p>
                                            A highly adaptable look that's simple
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-4.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Beautiful markup</h5>
                                        <p>
                                            Following BEM conventions for readability
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                        <span class="label">Hot</span>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/blog-1.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Built for teams</h5>
                                        <p>
                                            Ideal for large and small organisations
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-5.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Modern Aesthetic</h5>
                                        <p>
                                            A highly adaptable look that's simple
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-4.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Beautiful markup</h5>
                                        <p>
                                            Following BEM conventions for readability
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                        <span class="label">Hot</span>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/blog-1.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Built for teams</h5>
                                        <p>
                                            Ideal for large and small organisations
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-5.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Modern Aesthetic</h5>
                                        <p>
                                            A highly adaptable look that's simple
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-4.jpg"/>
                                    <div class="feature__body boxed boxed--border">
                                        <h5>Beautiful markup</h5>
                                        <p>
                                            Following BEM conventions for readability
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                        <span class="label">Hot</span>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 8
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/blog-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Startups</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/cowork-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Teams</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/education-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Freelancers</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/blog-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Startups</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/cowork-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Teams</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/education-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Freelancers</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/blog-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Startups</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/cowork-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Teams</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 col-6">
                                <a href="#" class="block">
                                    <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                        <div class="background-image-holder">
                                            <img alt="background" src="img/education-1.jpg"/>
                                        </div>
                                        <h4 class="pos-vertical-center">Freelancers</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 9
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class=" ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-6.jpg" class="border--round"/>
                                    <h4>Startups</h4>
                                    <p>
                                        Quickly launch a professional site &mdash; minimal effort, maximum impact.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-1.jpg" class="border--round"/>
                                    <h4>Agencies</h4>
                                    <p>
                                        Deliver results to your clients faster than ever with rapid page building.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/education-1.jpg" class="border--round"/>
                                    <h4>Freelancers</h4>
                                    <p>
                                        Stack is your silent partner, it does the heavy-lifting while you take the
                                        glory!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-6.jpg" class="border--round"/>
                                    <h4>Startups</h4>
                                    <p>
                                        Quickly launch a professional site &mdash; minimal effort, maximum impact.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-1.jpg" class="border--round"/>
                                    <h4>Agencies</h4>
                                    <p>
                                        Deliver results to your clients faster than ever with rapid page building.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/education-1.jpg" class="border--round"/>
                                    <h4>Freelancers</h4>
                                    <p>
                                        Stack is your silent partner, it does the heavy-lifting while you take the
                                        glory!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class=" bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-6.jpg" class="border--round"/>
                                    <h4>Startups</h4>
                                    <p>
                                        Quickly launch a professional site &mdash; minimal effort, maximum impact.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-1.jpg" class="border--round"/>
                                    <h4>Agencies</h4>
                                    <p>
                                        Deliver results to your clients faster than ever with rapid page building.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/education-1.jpg" class="border--round"/>
                                    <h4>Freelancers</h4>
                                    <p>
                                        Stack is your silent partner, it does the heavy-lifting while you take the
                                        glory!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class=" imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-6.jpg" class="border--round"/>
                                    <h4>Startups</h4>
                                    <p>
                                        Quickly launch a professional site &mdash; minimal effort, maximum impact.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/cowork-1.jpg" class="border--round"/>
                                    <h4>Agencies</h4>
                                    <p>
                                        Deliver results to your clients faster than ever with rapid page building.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="feature">
                                    <img alt="Image" src="img/education-1.jpg" class="border--round"/>
                                    <h4>Freelancers</h4>
                                    <p>
                                        Stack is your silent partner, it does the heavy-lifting while you take the
                                        glory!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 10
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<section class="unpad text-center ">
    <div class="row--gapless row no-gutters">
        <div class="col-md-4 col-6">
            <a href="#" class="block">
                <div class="feature feature-7 boxed imagebg" data-overlay="3">
                    <div class="background-image-holder">
                        <img alt="background" src="img/blog-1.jpg"/>
                    </div>
                    <h4 class="pos-vertical-center">Startups</h4>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-6">
            <a href="#" class="block">
                <div class="feature feature-7 boxed imagebg" data-overlay="3">
                    <div class="background-image-holder">
                        <img alt="background" src="img/cowork-1.jpg"/>
                    </div>
                    <h4 class="pos-vertical-center">Teams</h4>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-6">
            <a href="#" class="block">
                <div class="feature feature-7 boxed imagebg" data-overlay="3">
                    <div class="background-image-holder">
                        <img alt="background" src="img/education-1.jpg"/>
                    </div>
                    <h4 class="pos-vertical-center">Freelancers</h4>
                </div>
            </a>
        </div>
    </div>
    <!--end of row-->
</section>
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 11
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class="text-center ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Credit-Card2"></i>
                                        <span class="h5 color--primary">Payments and Billing</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Cloud"></i>
                                        <span class="h5 color--primary">Account Management</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Checked-User"></i>
                                        <span class="h5 color--primary">Permissions</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Handshake"></i>
                                        <span class="h5 color--primary">Integrations</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Credit-Card2"></i>
                                        <span class="h5 color--primary">Payments and Billing</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Cloud"></i>
                                        <span class="h5 color--primary">Account Management</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Checked-User"></i>
                                        <span class="h5 color--primary">Permissions</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Handshake"></i>
                                        <span class="h5 color--primary">Integrations</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Credit-Card2"></i>
                                        <span class="h5 color--primary">Payments and Billing</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Cloud"></i>
                                        <span class="h5 color--primary">Account Management</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Checked-User"></i>
                                        <span class="h5 color--primary">Permissions</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Handshake"></i>
                                        <span class="h5 color--primary">Integrations</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class="text-center imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Credit-Card2"></i>
                                        <span class="h5 color--primary">Payments and Billing</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Cloud"></i>
                                        <span class="h5 color--primary">Account Management</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Checked-User"></i>
                                        <span class="h5 color--primary">Permissions</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="block">
                                    <div class="feature boxed boxed--border border--round">
                                        <i class="icon--lg icon-Handshake"></i>
                                        <span class="h5 color--primary">Integrations</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 12
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class="text-center ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/cowork-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Built for teams</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/landing-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Modern Aesthetic</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-6.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Beautiful Markup</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/cowork-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Built for teams</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/landing-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Modern Aesthetic</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-6.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Beautiful Markup</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/cowork-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Built for teams</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/landing-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Modern Aesthetic</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-6.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Beautiful Markup</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class="text-center imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/cowork-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Built for teams</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/landing-1.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Modern Aesthetic</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-4">
                                <div class="feature feature-1">
                                    <img alt="Image" src="img/inner-6.jpg"/>
                                    <div class="feature__body boxed boxed--lg boxed--border">
                                        <h3>Beautiful Markup</h3>
                                        <p class="lead">
                                            Save time with a multitude of styled components designed to showcase your
                                            content
                                        </p>
                                        <a href="#">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 13
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class="text-center ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Mail-3 color--dark"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        for marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Air-Balloon color--dark"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        from a library over over 1,000
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Bacteria color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Fingerprint-2 color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Mail-3 color--dark"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        for marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Air-Balloon color--dark"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        from a library over over 1,000
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Bacteria color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Fingerprint-2 color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class="text-center bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Mail-3 color--dark"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        for marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Air-Balloon color--dark"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        from a library over over 1,000
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Bacteria color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Fingerprint-2 color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class="text-center imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Mail-3 color--dark"></i>
                                    <h4>Mailer Integrations</h4>
                                    <p>
                                        for marketing campaigns
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Air-Balloon color--dark"></i>
                                    <h4>Diverse Icons</h4>
                                    <p>
                                        from a library over over 1,000
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Bacteria color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="text-block">
                                    <i class="icon icon--lg icon-Fingerprint-2 color--dark"></i>
                                    <h4>Modular Design</h4>
                                    <p>
                                        for maximum flexibility
                                    </p>
                                </div>
                                <!--end feature-->
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>
<!--end of tabs container-->
<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="type--uppercase">Features Small 14
                    <i class="stack-down-dir"></i>
                </h6>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<div class="tabs-container text-center" data-content-align="left">
    <ul class="tabs">
        <li class="active">
            <div class="tab__title">
                <span class="h5">Light</span>
            </div>
            <div class="tab__content">
                <section class="features-small-14 text-center ">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8">
                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Preview"></i>
                                            <h4>Showcase Products</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Post-Mail2"></i>
                                            <h4>Mailer Integrations</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Fingerprint-2"></i>
                                            <h4>Infinitely Customisable</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Life-Safer"></i>
                                            <h4>Dedicated Support Team</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Duplicate-Window "></i>
                                            <h4>Modular Markup and Design</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Clock-Back"></i>
                                            <h4>Save Time</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Medium</span>
            </div>
            <div class="tab__content">
                <section class="features-small-14 text-center bg--secondary">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8">
                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Preview"></i>
                                            <h4>Showcase Products</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Post-Mail2"></i>
                                            <h4>Mailer Integrations</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Fingerprint-2"></i>
                                            <h4>Infinitely Customisable</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Life-Safer"></i>
                                            <h4>Dedicated Support Team</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Duplicate-Window "></i>
                                            <h4>Modular Markup and Design</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Clock-Back"></i>
                                            <h4>Save Time</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Dark</span>
            </div>
            <div class="tab__content">
                <section class="features-small-14 text-center bg--dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8">
                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Preview"></i>
                                            <h4>Showcase Products</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Post-Mail2"></i>
                                            <h4>Mailer Integrations</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Fingerprint-2"></i>
                                            <h4>Infinitely Customisable</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Life-Safer"></i>
                                            <h4>Dedicated Support Team</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Duplicate-Window "></i>
                                            <h4>Modular Markup and Design</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Clock-Back"></i>
                                            <h4>Save Time</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
        <li>
            <div class="tab__title">
                <span class="h5">Image</span>
            </div>
            <div class="tab__content">
                <section class="features-small-14 text-center imagebg" data-overlay='4'>
                    <div class="background-image-holder">
                        <img alt="background" src="img/hero-1.jpg"/>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8">
                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Preview"></i>
                                            <h4>Showcase Products</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Post-Mail2"></i>
                                            <h4>Mailer Integrations</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Fingerprint-2"></i>
                                            <h4>Infinitely Customisable</h4>
                                        </div>
                                        <!---end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Life-Safer"></i>
                                            <h4>Dedicated Support Team</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Duplicate-Window "></i>
                                            <h4>Modular Markup and Design</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-block">
                                            <i class="icon color--dark icon-Clock-Back"></i>
                                            <h4>Save Time</h4>
                                        </div>
                                        <!--end feature-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
        </li>
    </ul>
</div>