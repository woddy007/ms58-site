<footer>
    <div class="tm-footer">
        <div class="uk-container">
            <div class="tm-footer__content">
                <div class="tm-footer__logo">
                    <img src="/static/app/icons/logo.png" class="tm-footer__logo-image">
                    <div class="tm-footer__logo-label">
                        <p>Мамина-</p>
                        <p>Сибиряка, 58</p>
                    </div>
                </div>

                <ul class="tm-footer__menu">
                    <li class="tm-footer__menu-item">
                        <a href="#">Аренда и продажа</a>
                    </li>
                    <li class="tm-footer__menu-item">
                        <a href="#">Спецпредложения</a>
                    </li>
                    <li class="tm-footer__menu-item">
                        <a href="#">О нас</a>
                    </li>
                    <li class="tm-footer__menu-item">
                        <a href="#">Новости</a>
                    </li>
                    <li class="tm-footer__menu-item">
                        <a href="#">Контакты</a>
                    </li>
                </ul>

                <div class="tm-footer__right">
                    <a href="tel:<?= cms('phone') ?>" class="tm-footer__phone"><?= cms('phone') ?></a>
                    <span class="tm-footer__text"><?= cms('address') ?></span>
                    <a href="#" class="tm-footer__text"><?= cms('email') ?></a>
                </div>
            </div>
        </div>
    </div>
</footer>
