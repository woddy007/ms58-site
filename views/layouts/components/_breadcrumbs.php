<?php
/* @var $this luya\web\View */

use yii\bootstrap\Alert;
use yii\helpers\Url;

/* @var $content string */

if (Yii::$app->controller->route == 'cms/default/index') {
    if (Yii::$app->menu->current->isHome) {
        return;
    }

    foreach ($items = Yii::$app->menu->current->teardown as $item) {
        /* @var $item \luya\cms\menu\Item */
        if ($item != end($items)) {
            $this->params['breadcrumbs'][] = ['url' => $item->link, 'label' => $item->title];
        } else {
            $this->params['breadcrumbs'][] = $item->title;
        }

    }
}
?>
<div role="navigation" class="uk-section uk-section-xsmall">
    <div class="uk-container">
        <ul class="uk-breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemscope itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                <a href="/" itemprop="item">
                    <span itemprop="name">Главная</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <?php if (isset($this->params['breadcrumbs'])): ?>
                <?php foreach ($this->params['breadcrumbs'] as $index => $crumb): ?>

                    <?php if ((1 + $index) !== count($this->params['breadcrumbs'])): ?>
                        <li itemscope itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a href="<?= Url::toRoute($crumb['url']) ?>" itemprop="item">
                                <span itemprop="name"><?= $crumb['label'] ?></span>
                            </a>
                            <meta itemprop="position" content="<?= $index + 2 ?>">
                        </li>
                    <?php else: ?>
                        <li class="uk-active" itemscope itemprop="itemListElement"
                            itemtype="http://schema.org/ListItem">
                                    <span itemprop="item">
                                        <span itemprop="name"><?= $crumb ?></span>
                                    </span>
                            <meta itemprop="position" content="<?= $index + 2 ?>">
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

        </ul>
    </div>
</div>
