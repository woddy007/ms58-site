<?php

$homePage = false;

if (Yii::$app->controller->route == 'cms/default/index') {
    if (Yii::$app->menu->current->isHome) {
        $homePage = true;
    }
}

?>

<header class="<?= ( $homePage )? 'tm-header-home': '' ?>">
    <div class="tm-header" data-uk-sticky="top: 150">
        <div class="uk-container">
            <div class="tm-header__content">
                <a href="/" class="tm-logo tm-header__logo">
                    <img class="tm-logo__image" src="/static/app/icons/logo.png">
                    <div class="tm-logo__info">
                        <div class="tm-logo__title">
                            <p>Мамина-</p>
                            <p>Сибиряка, 58</p>
                        </div>
                        <div class="tm-logo__floor">
                            Офисы на 10 этаже
                        </div>
                    </div>
                </a>

                <div class="tm-header__repulsive"></div>

                <a href="tel: <?= cms('phone') ?>" class="tm-header__phone">
                    <span data-uk-icon="receiver"></span>
                    <?= cms('phone') ?>
                </a>

                <button class="tm-header__button-login uk-button uk-button-primary">Войти</button>

                <button class="tm-header__button-menu">
                    <span data-uk-icon="menu"></span>
                </button>
            </div>
        </div>
    </div>
</header>
