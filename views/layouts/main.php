<?php
/**
 * @var string $content
 * @var \luya\web\View $this
 */

?>

<?php $this->beginContent('@app/views/layouts/_clear.php'); ?>


<?= $this->render('components/_header'); ?>
<?= $this->render('components/_breadcrumbs'); ?>
<?= $content ?>
<?= $this->render('components/_footer'); ?>

<?= \sitis\shop\frontend\vue\widgets\main\Modal::widget() ?>

<?= \sitis\plugins\frontend\widgets\vue\inputs\Select::widget() ?>
<?= \sitis\uniform\widgets\UniModalWidget::widget() ?>
<?= \sitis\uniform\widgets\UniModalWidget::widget([
    'message' => true,
    'email' => true,
    'required' => ['message'],
    'settings' => [
        'id' => 'request-another',
    ]
]) ?>


<?php $this->endContent(); ?>
