<?php

/* @var $this luya\web\View */

/* @var $content string */

use sitis\config\assets\WebpackAsset;
use sitis\config\models\Counter;

WebpackAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->composition->language; ?>">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $this->title; ?> <?= \Yii::$app->cConfig->getItemValue('suffixDivider') ?> <?= \Yii::$app->cConfig->getItemValue('suffixTitle') ?></title>
    <?php $this->head() ?>
    <?= Counter::getHeaderCounters() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
<?= Counter::getFooterCounters() ?>
</body>
</html>
<?php $this->endPage() ?>
