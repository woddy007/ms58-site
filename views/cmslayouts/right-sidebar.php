<div class="uk-grid" data-uk-grid>
    <div class="uk-width-1-1 uk-width-3-4@m"><?= $placeholders['content'] ?></div>
    <div class="uk-width-1-1 uk-width-1-4@m"><?= $placeholders['sidebar'] ?></div>
</div>
