<?php

/**
 * @var yii\web\View $this
 * @var \yii\data\DataProviderInterface $dataProvider
 * @var \sitis\shop\core\entities\Shop\FilterPage $filterPage
 * @var \sitis\shop\core\forms\Shop\Search\SearchForm $searchForm
 * @var \sitis\shop\core\wishlist\Wishlist $wishlist
 */


$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $filterPage->title;
?>

<h2><?= $filterPage->title ?></h2>
<?= $filterPage->text ?>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider,
    'wishlist' => $wishlist,
]) ?>

