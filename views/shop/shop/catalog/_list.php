<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $wishlist \sitis\shop\core\wishlist\Wishlist */

$page = $dataProvider->getPagination()->page + 1;
?>

<div class="product-list">


    <div class="uk-flex uk-flex-middle uk-flex-between uk-margin-medium-bottom">
        <div>
                <span>Показаны <?= $begin = $dataProvider->getPagination()->getPage() * $dataProvider->getPagination()->pageSize + 1 ?>
                    - <?= $begin + $dataProvider->getCount() - 1; ?> из <?= $dataProvider->getTotalCount() ?>
                    товаров</span>
        </div>
        <div class="uk-flex">
            <div class="uk-margin-right">
                <div uk-form-custom="target: > * > span:first-child">
                    <select class="uk-select uk-margin-small-left uk-margin-small-right"
                            onchange="location = this.value;">
                        <?php
                        $values = [20, 50];
                        $current = $dataProvider->getPagination()->getPageSize();
                        ?>
                        <?php foreach ($values as $value): ?>
                            <option value="<?= Html::encode(Url::current(['per-page' => $value])) ?>"
                                    <?php if ($current == $value): ?>selected="selected"<?php endif; ?>>
                                Показывать по <?= $value ?> шт.
                            </option>
                        <?php endforeach; ?>
                    </select>
                    <button class="uk-button uk-button-default" type="button" tabindex="-1">
                        <span></span>
                        <span uk-icon="icon: chevron-down"></span>
                    </button>
                </div>
            </div>
            <div>
                <div uk-form-custom="target: > * > span:first-child">
                    <select class="uk-select uk-margin-small-left uk-margin-small-right tm-filter-select__sort"
                            aria-label="Сортировка"
                            aria-describedby="sort"
                            onchange="location = this.value;">
                        <?php
                        $values = [
                            '' => 'По умолчанию',
                            'name' => 'Название (А - Я)',
                            '-name' => 'Название (Я - А)',
                            'price' => 'Стоимость (Дешевле)',
                            '-price' => 'Стоимость (Дороже)',
                        ];
                        $current = Yii::$app->request->get('sort');
                        ?>
                        <?php foreach ($values as $value => $label): ?>
                            <option value="<?= Html::encode(Url::current(['sort' => $value ?: null])) ?>"
                                    <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                    <button class="uk-button uk-button-default" type="button" tabindex="-1">
                        <span></span>
                        <span uk-icon="icon: chevron-down"></span>
                    </button>
                </div>
            </div>
        </div>


    </div>


    <div class="list-view uk-child-width-1-4 uk-grid uk-grid-small" data-page="<?= $page ?>" data-uk-grid>
        <?php foreach ($dataProvider->getModels() as $product): ?>
            <?= $this->render('_product', [
                'product' => $product,
                'wishlist' => $wishlist,
            ]) ?>
        <?php endforeach; ?>
    </div>

    <div class="uk-margin-medium product-pagination uk-card uk-flex uk-flex-between uk-flex-middle">
        <?= \sitis\plugins\frontend\widgets\UniPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'maxButtonCount' => 6,
            'nextPageLabel' => 'Вперед >',
            'nextPageCssClass' => 'next',
            'prevPageLabel' => '< Назад',
            'prevPageCssClass' => 'prev',
            'linkOptions' => ['class' => ''],
            'activePageCssClass' => 'active',
            'options' => ['class' => 'uk-pagination page-pagination'],

            'morePageButton' => 'Показать еще',
            'morePageButtonOptions' => ['class' => ''],
            'morePageContainerOptions' => ['class' => ''],
            'paginationSelector' => '.product-pagination',
            'addAfterSelector' => '.product-list',
            'productSelector' => '.product-link',
        ]) ?>
        <span>Показаны <?= $begin = $dataProvider->getPagination()->getPage() * $dataProvider->getPagination()->pageSize + 1 ?>
            - <?= $begin + $dataProvider->getCount() - 1; ?> из <?= $dataProvider->getTotalCount() ?> товаров</span>
    </div>
</div>