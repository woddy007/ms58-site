<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */

/* @var $searchForm \sitis\shop\core\forms\Shop\Search\SearchForm */

$this->title = 'Поиск';

$this->params['breadcrumbs'][] = $this->title;
$this->params['title'] = $this->title;
?>

<section class="uk-container">
    <h2>Каталог</h2>
    <?= $this->render('_list', [
        'dataProvider' => $provider
    ]) ?>
</section>


