<?php

/* @var $this yii\web\View */

use sitis\seo\models\Template;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $category \sitis\shop\core\entities\Shop\Category */
/* @var $searchForm \sitis\shop\core\forms\Shop\Search\SearchForm */
/* @var $wishlist \sitis\shop\core\wishlist\Wishlist */

$category->seo_title = $category->meta->title;
$category->seo_keywords = $category->meta->keywords;
$category->seo_description = $category->meta->description;
Template::registerSeoMetaTags($category, $this);

$this->params['active_category'] = $category;
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
foreach ($category->parents as $parent) {
    if (!$parent->isRoot()) {
        $this->params['breadcrumbs'][] = ['label' => $parent->name, 'url' => ['category', 'id' => $parent->id]];
    }
}
$this->params['breadcrumbs'][] = $category->name_one ?? $category->name;
?>

<div class="uk-container">

    <h1><?= $category->name ?></h1>

    <div class="uk-grid" data-uk-grid>
        <div class="uk-width-1-5">
            <?= $this->render('_filter', [
                'dataProvider' => $dataProvider,
                'searchForm' => $searchForm,
            ]) ?>
        </div>

        <div class="uk-width-expand">

            <?= $this->render('_list', [
                'dataProvider' => $dataProvider,
                'wishlist' => $wishlist,
            ]) ?>
        </div>
    </div>
</div>
