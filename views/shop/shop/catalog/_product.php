<?php

/**
 * @var $this yii\web\View
 * @var $product sitis\shop\core\entities\Shop\Product\Product
 * @var $wishlist \sitis\shop\core\wishlist\Wishlist
 */

use sitis\blocks\components\helpers\ImageHelper;
use yii\helpers\Url;

$url = Url::to(['/shop/shop/catalog/product', 'slug' => $product->slug]);
$wishClass = $wishlist->checkProduct($product->id) ? ' wish' : '';
?>

<div>
    <div class="uk-card uk-card-default uk-position-relative uk-card-body uk-card-small">
        <img class="" src="<?= ImageHelper::getImage($product->main_photo_id, 'product-catalog') ?>"
             alt="<?= $product->name ?>">
        <div class="uk-margin-small-top">
            <a href="<?= $url ?>" class="uk-h5 product-link"><?= $product->name ?></a>
            <p>
                <?= $product->price_old ? '<span style="text-decoration: line-through">' . $product->price_old . '₽</span> ' . $product->price_new : $product->price_new ?>
                ₽
            </p>
            <div class="uk-button-group uk-width-1-1 uk-flex-around">
                <a href="#" class="uk-icon-button uk-margin-small-right" uk-icon="plus-circle"
                   onclick="cartVm.quickBuy(<?= $product->id ?>); return false;"></a>
                <a href="#" class="uk-icon-button  uk-margin-small-right" uk-icon="heart"
                   onclick="profileVm.toggleWishlist(<?= $product->id ?>, this); return false;"></a>
                <a href="#" class="uk-icon-button" uk-icon="cart"
                   onclick="cartVm.add(<?= $product->id ?>, 1); return false;"></a>
            </div>
        </div>
        <?php if ($product->modifications): ?>
            <div class="uk-overlay uk-overlay-default uk-position-top uk-padding-small uk-text-small">
                <div>
                    <div class="uk-text-bold">Размеры:</div>
                    <?php foreach ($product->modifications as $modification): ?>
                        <span class="uk-badge"><?= $modification->variant->value ?></span>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>