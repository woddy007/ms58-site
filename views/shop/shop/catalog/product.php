<?php

/* @var $this yii\web\View */
/* @var $product sitis\shop\core\entities\Shop\Product\Product */
/* @var $cartForm \sitis\shop\core\forms\Shop\AddToCartForm */

/* @var $reviewForm \sitis\shop\core\forms\Shop\ReviewForm */

use sitis\blocks\components\helpers\ImageHelper;
use sitis\shop\core\entities\Shop\Characteristic\Group;
use yii\helpers\Url;


$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
foreach ($product->category->parents as $parent) {
    if (!$parent->isRoot()) {
        $this->params['breadcrumbs'][] = ['label' => $parent->name, 'url' => ['category', 'id' => $parent->id]];
    }
}
$this->params['breadcrumbs'][] = ['label' => $product->category->name, 'url' => ['category', 'id' => $product->category->id]];
$this->params['breadcrumbs'][] = $product->name;
$this->params['active_category'] = $product->category;
?>

<div class="uk-section">


    <div class="uk-container">
        <div class="uk-child-width-1-1 uk-child-width-1-2@m uk-margin-medium-bottom" data-uk-grid>
            <div>
                <div class="" data-uk-slideshow="animation: slide;ratio: 4:3">
                    <ul class="uk-slideshow-items" data-uk-lightbox="toggle:li">
                        <?php foreach ([0, 1, 2, 3, 4] as $i): ?>
                            <li data-href="<?= ImageHelper::getImage($product->main_photo_id) ?>">
                                <img class="zoom" src="<?= ImageHelper::getImage($product->main_photo_id) ?>" alt=""
                                     data-uk-cover data-zoom="<?= ImageHelper::getImage($product->main_photo_id) ?>">
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="uk-thumbnav uk-child-width-1-4 uk-child-width-1-6@m uk-grid uk-grid-small uk-margin-small-top"
                         data-uk-grid>
                        <?php foreach ([0, 1, 2, 3, 4] as $i): ?>
                            <a href="#" data-uk-slideshow-item="<?= $i ?>">
                                <div>
                                    <img src="<?= ImageHelper::getImage($product->main_photo_id) ?>" alt="">
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="zoom-zoom uk-position-relative">
                <h1 class="uk-h1 uk-text-bold"><?= $product->name ?></h1>
                <div class="uk-flex">
                    <p class="uk-text-bold uk-text-large uk-margin-remove"><?= $product->price_new ?> р.</p>
                    <?php if ($product->price_old): ?>
                        <p class="uk-text-danger uk-margin-remove"
                           style="text-decoration: line-through"><?= $product->price_old ?> р.</p>
                    <?php endif; ?>
                </div>
                <div class="uk-margin-medium-top">
                    <span>Бренд: </span>
                    <span><?= $product->brand->name ?></span>
                </div>
                <div class="">
                    <span>Наличие: </span>
                    <span class="uk-text-success"><?= $product->quantity > 0 ? 'В наличии' : 'Нет в наличии' ?></span>
                </div>
                <div class="uk-margin-medium-top">
                    <?= $product->short_description ?>
                </div>
                <hr>
                <div class="uk-flex uk-margin-medium-top">
                    <?= \sitis\shop\frontend\vue\widgets\main\BuyForm::widget(['product' => $product]) ?>
                </div>
                <div class="uk-margin">
                    <div class="uk-padding-small uk-background-muted uk-border-rounded">
                        <div class="uk-grid-small uk-child-width-1-2 uk-text-small" data-uk-grid>
                            <div>
                                <div class="uk-grid-collapse" data-uk-grid>
                                    <span class="uk-margin-right" data-uk-icon="icon:home; ratio:1.5"></span>
                                    <div>
                                        <div class="uk-text-bolder">Доставка</div>
                                        <div class="uk-text-xsmall uk-text-muted">Бесплатно от 3 000 рублей</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="uk-grid-collapse" data-uk-grid>
                                    <span class="uk-margin-right" data-uk-icon="icon:location; ratio:1.5"></span>
                                    <div>
                                        <div class="uk-text-bolder">Самовывоз</div>
                                        <div class="uk-text-xsmall uk-text-muted">10000 пунктов выдачи</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-margin-medium-top">
                    <span class="uk-margin-small-right">Категория:</span><a
                            href="<?= Url::to(['/shop/shop/catalog/category', 'id' => $product->category->id]) ?>"><?= $product->category->name ?></a>
                </div>
                <div class="uk-margin-small-top">
                    <span class="uk-margin-small-right">Теги:</span>
                    <?php foreach ($product->tags as $tag): ?>
                        <a href="<?= Url::to(['/shop/shop/catalog/tag', 'slug' => $tag->slug]) ?>"><?= $tag->name ?></a>
                    <?php endforeach; ?>
                </div>
                <?php foreach (Group::find()->all() as $group): ?>
                    <div class="uk-margin-small-top">
                        <span class="uk-margin-small-right"><?= $group->name ?>:</span>
                        <?php foreach ($group->assignedCharacteristics as $characteristic): ?>
                            <span><?= $characteristic->name ?></span>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="uk-section">

            <div class="uk-width-1-1 uk-margin-medium-bottom">
                <ul class="uk-flex-center" data-uk-tab data-uk-switcher>
                    <li class="uk-active"><a class="uk-text-bold" href="#">Описание</a></li>
                    <li><a class="uk-text-bold" href="#">Характеристики</a></li>
                    <li><a class="uk-text-bold" href="#">Отзывы</a></li>
                </ul>

                <ul class="uk-switcher uk-margin">
                    <li><?= $product->description ?></li>
                    <li>
                        Hello again!
                    </li>
                    <li>Bazinga!</li>
                </ul>

            </div>
            <div class="uk-width-1-1 uk-margin-medium-bottom">
                <h4 class="uk-heading-line"><span class="uk-text-bold">Похожие товары</span></h4>
                <div uk-slider>
                    <div class="uk-position-relative">
                        <div class="uk-slider-container">
                            <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m uk-grid">
                                <?php foreach (\sitis\shop\core\entities\Shop\Product\Product::find()->limit(10)->all() as $related): ?>
                                    <li>
                                        <?= $this->render('@app/views/shop/shop/catalog/_product', [
                                            'product' => $related,
                                            'wishlist' => $wishlist
                                        ]) ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="uk-hidden@s uk-light">
                            <a class="uk-position-center-left uk-position-small" href="#" data-uk-slidenav-previous
                               uk-slider-item="previous"></a>
                            <a class="uk-position-center-right uk-position-small" href="#" data-uk-slidenav-next
                               uk-slider-item="next"></a>
                        </div>
                        <div class="uk-visible@s">
                            <a class="uk-position-center-left-out uk-position-small" href="#" data-uk-slidenav-previous
                               uk-slider-item="previous"></a>
                            <a class="uk-position-center-right-out uk-position-small" href="#" data-uk-slidenav-next
                               uk-slider-item="next"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

