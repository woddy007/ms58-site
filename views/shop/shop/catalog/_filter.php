<?php
/**
 * Created by PhpStorm.
 * User: vlasov
 * Date: 14.02.2018
 * Time: 16:30
 */

/**
 * @var $this \luya\web\View
 * @var $category \sitis\shop\core\entities\Shop\Category
 * @var $searchForm \sitis\shop\core\forms\Shop\Search\SearchForm
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$action = Url::to();


if (($pos = strpos($action, '?')) !== false) {
    $action = substr($action, 0, $pos);
}

$filterUrl = Url::to(['filter-ajax', 'id' => $searchForm->category]);

?>

<div id="search-form" class="d-flex align-items-end mb-30 flex-wrap" data-replace-selector=".product-list">

    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'id' => 'products-search-form',
        'action' => $action,
        'enableClientScript' => false,
        'options' => ['ref' => 'form', 'data-filter-url' => $filterUrl],
    ]); ?>

    <div class="catalog-filters">
        <?php if ($brands = $searchForm->brandsList()): ?>
            <ul uk-accordion="multiple: true">
                <li class="uk-open">
                    <a class="uk-accordion-title" href="#">Бренд</a>
                    <div class="uk-accordion-content">
                        <div class="uk-panel uk-panel-scrollable uk-resize-vertical">
                            <sitis-select
                                    name="<?= $searchForm->formName() . 'brand[]' ?>"
                                    :options='<?= json_encode($brands) ?>'
                                    :default='<?= json_encode($searchForm->brand) ?>'
                                    multiple
                            ></sitis-select>
                        </div>
                    </div>
                </li>
            </ul>
            <!--                    --><? //= Html::activeDropDownList($searchForm, 'brand', $brands, ['multiple' => 'miltiple', 'class' => 'uk-select']) ?>
        <?php endif; ?>

        <ul uk-accordion="multiple: true">
            <?php $key = 0; ?>
            <?php foreach ($searchForm->values as $i => $value): ?>
                <?php if ($variants = $searchForm->characteristicVariants($value->getId())): ?>
                    <?php $availableVariants = $this->context->products->getAvailableCharacteristicVariants($value->getId(),
                        $searchForm); ?>

                    <li class="<?= $key++ === 0 || 1 ? 'uk-open' : '' ?>">
                        <a class="uk-accordion-title" href="#"><?= $value->getCharacteristicName() ?></a>
                        <div class="uk-accordion-content">
                            <div class="uk-panel uk-panel-scrollable uk-resize-vertical">
                                <sitis-select
                                        name="<?= $value->formName() . '[' . $i . '][equal][]' ?>"
                                        :options='<?= json_encode($variants) ?>'
                                        :availables='<?= json_encode($availableVariants) ?>'
                                        :default='<?= json_encode($value->equal) ?>'
                                        ref="sitis-select-values-<?= $value->getId() ?>"
                                        @update="submit"
                                        multiple
                                ></sitis-select>
                            </div>
                        </div>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>

        <?php if ($modificationGroups = $searchForm->modificationGroupList()): ?>
            <hr>
            <ul uk-accordion="multiple: true">
                <?php $key = 0; ?>
                <?php foreach ($modificationGroups as $id => $name): ?>
                    <?php if ($modifications = $searchForm->modificationsList($id)): ?>
                        <?php $availableModifications = $this->context->products->getAvailableModificationVariants($id,
                            $searchForm); ?>
                        <li>
                            <a class="uk-accordion-title" href="#"><?= $name ?></a>
                            <div class="uk-accordion-content">
                                <div class="uk-panel uk-panel-scrollable uk-resize-vertical">

                                    <sitis-select
                                            name="<?= $searchForm->formName() . 'modifications[]' ?>"
                                            :options='<?= json_encode($modifications) ?>'
                                            :availables='<?= json_encode($availableModifications) ?>'
                                            :default='<?= json_encode($searchForm->modifications) ?>'
                                            ref="sitis-select-modifications-<?= $id ?>"
                                            @update="submit"
                                            multiple
                                    ></sitis-select>
                                </div>
                            </div>
                        </li>
                        <? //= Html::activeDropDownList($searchForm, 'modifications', $modifications, ['multiple' => 'miltiple', 'class' => 'uk-select']) ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <hr>
        <?php endif; ?>

        <?php if ($prices = $searchForm->getMinMaxPrice()): ?>
            <?php if ($prices['min'] != $prices['max']): ?>
                <label><?= 'Цена (' . $prices['min'] . ' - ' . $prices['max'] . ')' ?></label>
                <?= Html::activeHiddenInput($searchForm, 'price', ['ref' => 'price']) ?>
                <vue-slider
                        :min="<?= floor($prices['min'] / 100) * 100 ?>"
                        :max="<?= ceil($prices['max'] / 100) * 100 ?>"
                        :interval="100"
                        :value='<?= json_encode($searchForm->getPriceArray()) ?>'
                        @input="(value) => { setValueByRef('price', value) }"
                ></vue-slider>
            <?php endif; ?>
        <?php endif; ?>

        <div class="uk-margin">
            <?= Html::activeCheckbox($searchForm, 'special',
                ['label' => 'Спец. предложение', 'class' => 'uk-checkbox']) ?>
        </div>

        <?= Html::button('Поиск', ['@click' => 'submit', 'class' => 'uk-button uk-button-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>


</div>