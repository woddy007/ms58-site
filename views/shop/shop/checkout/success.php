<?php

/* @var $this yii\web\View */
/* @var $order \sitis\shop\core\entities\Shop\Order\Order */

/* @var $paymentForm string */

$this->title = 'Заказ оформлен';
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/shop/shop/catalog/index']];
$this->params['breadcrumbs'][] = ['label' => 'Заказ', 'url' => ['/shop/shop/cart/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabinet-index">
    <div class="uk-container">

        <div uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <h3>Заказ №<?= $order->id ?></h3>

            <?php if ($paymentForm): ?>
                <div>
                    <?php echo $paymentForm; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
