<?php

/* @var $this yii\web\View */
/* @var $cart \sitis\shop\core\cart\Cart */

/* @var $model \sitis\shop\core\forms\Shop\Order\OrderForm */

use yii\helpers\Html;

$this->title = 'Оформление заказа';
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/shop/shop/catalog/index']];
$this->params['breadcrumbs'][] = ['label' => 'Заказ', 'url' => ['/shop/shop/cart/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-width">
    <div class="cabinet-index">
        <h1><?= Html::encode($this->title) ?></h1>
        <?= \sitis\shop\frontend\vue\widgets\main\Checkout::widget() ?>
    </div>
</div>
