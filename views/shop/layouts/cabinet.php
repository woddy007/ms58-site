<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

?>
<?php $this->beginContent('@app/views/layouts/main.php') ?>
<div class="uk-container">
    <h3 class="uk-h3 uk-text-bold tm-heading-divider">ЛИЧНЫЙ КАБИНЕТ</h3>
    <div class="uk-grid uk-margin-medium-top" data-uk-grid>
        <div class="uk-width-1-1 uk-width-1-4@m">
            <?= \yii\widgets\Menu::widget([
                'linkTemplate' => '<a href="{url}">{label}</a>',
                'submenuTemplate' => "\n<ul class='uk-nav-sub'>\n{items}\n</ul>\n",
                'items' => [
                    ['label' => 'Избранное', 'url' => ['/shop/cabinet/wishlist/index']],
                    ['label' => 'Личные данные', 'url' => ['/shop/cabinet/profile/index'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Изменить пароль', 'url' => ['/shop/cabinet/profile/password'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'История заказов', 'url' => ['/shop/cabinet/order/index'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Выход', 'url' => '/logout', 'visible' => !Yii::$app->user->isGuest],
                ],
                'activeCssClass' => 'uk-active uk-open',
                'activateParents' => true,
                'activateItems' => true,
                'options' => [
                    'class' => 'uk-nav uk-nav-default uk-nav-parent-icon',
                    'data-uk-nav' => 'multiple: true',
                ],
            ])
            ?>
        </div>
        <div class="uk-width-1-1 uk-width-3-4@m">
            <?= $content ?>
        </div>
    </div>
</div>
<?php $this->endContent() ?>
