<?php
/* @var $this \yii\web\View */

/* @var $content string */


?>

<?php if (isset($this->params['category'])): ?>
    <?php $category = $this->params['category']; ?>
    <?php /** @var $category \sitis\shop\core\entities\Shop\Category */ ?>
<?php endif; ?>

<?php $this->beginContent('@app/views/layouts/main.php') ?>
<?= $content ?>
<?php $this->endContent() ?>
