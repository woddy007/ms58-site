<?php

/* @var $this yii\web\View */

/* @var $wishlist \sitis\shop\core\wishlist\Wishlist */

use yii\helpers\Html;

$this->title = 'Избранное';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['cabinet/profile/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>
<div class="uk-margin-medium-top uk-margin-medium-bottom" data-uk-grid>
    <?= $this->render('@app/views/shop/shop/catalog/_list', [
        'dataProvider' => $dataProvider,
        'wishlist' => $wishlist,
    ]) ?>
</div>
