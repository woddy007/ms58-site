<?php

use sitis\shop\core\entities\Shop\Order\Order;
use sitis\shop\core\helpers\OrderHelper;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['cabinet/profile/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="section-sub-title"><?= Html::encode($this->title) ?></h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'id',
            'value' => function (Order $model) {
                return Html::encode($model->id);
            },
            'label' => '№ Заказа',
            'format' => 'raw',
        ],
        [
            'attribute' => 'items',
            'value' => function (Order $model) {
                return $model->getItems()->count();
            },
            'label' => 'Кол-во товаров',
            'format' => 'raw',
        ],
        'created_at:datetime',
        [
            'attribute' => 'status',
            'label' => 'Статус',
            'value' => function (Order $model) {
                return OrderHelper::statusLabel($model->current_status);
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'cost',
            'value' => function (Order $model) {
                return $model->cost . ' руб.';
            },
            'format' => 'raw',
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a('Посмотреть', ['view', 'id' => $model->id], ['class' => 'uk-button uk-button-default']);
                },
            ],
        ],
    ],
    'layout' => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'uk-table uk-form uk-table-condensed uk-table-striped uk-table-hover uk-table-middle',
    ],
]); ?>
