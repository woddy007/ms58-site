<?php

use sitis\shop\core\helpers\OrderHelper;
use sitis\shop\core\helpers\PriceHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $order \sitis\shop\core\entities\Shop\Order\Order
 */

$this->title = 'Заказ № ' . $order->id;

$this->params['breadcrumbs'][] = ['label' => 'Кабинет', 'url' => ['cabinet/profile/index']];
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1 class="text-center m-4"><?= Html::encode($this->title) ?></h1>

    <div class="card">

        <?= DetailView::widget([
            'model' => $order,
            'options' => ['class' => 'uk-table uk-table-striped detail-view'],
            'attributes' => [

                'created_at:datetime',
                [
                    'attribute' => 'current_status',
                    'value' => OrderHelper::statusLabel($order->current_status),
                    'format' => 'raw',
                ],
                'delivery_method_name',
                'cost:currency',
                'note:ntext',
            ],
        ]) ?>
    </div>

    <h2 class="text-center m-5">Состав заказа</h2>
    <div class="card">

        <div class="table-responsive">
            <table class="uk-table uk-table-striped">
                <thead>
                <tr>
                    <th class="text-left">Товар</th>
                    <th class="text-left">Артикул</th>
                    <th class="text-left">Количество</th>
                    <th class="text-right">Цена ед.</th>
                    <th class="text-right">Итого</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order->items as $item): ?>
                    <tr>
                        <td class="uk-table-link">
                            <?= Html::a($item->product_name, $item->product->getUrl(), ['class' => 'uk-link-reset']) ?>
                        </td>
                        <td class="text-left">
                            <?= Html::encode($item->modification_code) ?>
                            <?= Html::encode($item->modification_name) ?>
                        </td>
                        <td class="text-left">
                            <?= $item->quantity ?>
                        </td>
                        <td class="text-right"><?= PriceHelper::format($item->price) ?> руб.</td>
                        <td class="text-right"><?= PriceHelper::format($item->getCost()) ?> руб.</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>


</div>