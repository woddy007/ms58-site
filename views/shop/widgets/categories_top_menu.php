<?php


?>

<div class="uk-grid uk-child-width-1-1 uk-child-width-1-3@m uk-margin-large" data-uk-grid>
    <?php foreach ($menu as $item): ?>
        <div class="uk-margin-medium-bottom">
            <div class="uk-h2 uk-heading-divider">
                <a class="uk-link-reset" href="<?= \yii\helpers\Url::to($item['url']) ?>">
                    <?= $item['label'] ?>
                </a>
            </div>
            <?php if (isset($item['items'])): ?>

                <div>
                    <?php foreach ($item['items'] as $sub_item): ?>
                        <a class="uk-margin-small-right"
                           href="<?= \yii\helpers\Url::to($sub_item['url']) ?>"><?= $sub_item['label'] ?></a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>
