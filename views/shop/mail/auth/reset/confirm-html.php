<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \sitis\shop\core\entities\User\User */

$resetLink = \luya\cms\helpers\Url::toRoute(['auth/reset/confirm', 'token' => $user->password_reset_token], true);
?>
<div class="password-reset">
    <p>Здравствуйте, <?= Html::encode($user->username) ?>,</p>

    <p>Откройте ссылку для сброса пароля:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
