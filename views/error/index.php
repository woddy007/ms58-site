<?php

/**
 * @var $this \yii\web\View
 * @var $exception \Exception|null
 */

$this->beginContent('@app/views/layouts/main.php');
?>
<section class="content-width cms-page container">
    <div class="breadcrumbs">
        <div class="breadcrumbs__content container-fluid">
            <div class="row">
                <div class="col">
                    <a class="breadcrumbs__link" href="/">Главная</a>
                    <span class="breadcrumbs__curr">Ошибка <?= $exception !== null ? $exception->statusCode : '' ?></span>
                </div>
            </div>
        </div>
    </div>
    <?php if ($exception !== null): ?>
        <?php if ($error_page = \luya\cms\models\NavItem::findOne(['alias' => $exception->statusCode])): ?>
            <?= $error_page->type->renderPlaceholder('content') ?>
        <?php else: ?>
            <h2><?= $exception->getMessage() ?></h2>
            <h3 class="red">Код ошибки: <?= $exception->statusCode ?></h3>
            <p>Произошла ошибка при обработки вашего запроса.</p>
            <p>Пожалуйста, свяжитесь с нами, если вы считаете, что это ошибка сервера.</p>
        <?php endif; ?>
    <?php else: ?>
        <p>Произошла ошибка при обработки вашего запроса.</p>
        <p>Пожалуйста, свяжитесь с нами, если вы считаете, что это ошибка сервера.</p>
    <?php endif; ?>
</section>
<?php $this->endContent(); ?>

