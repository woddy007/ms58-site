<?php

use yii\widgets\ActiveForm;
use luya\helpers\Html;
use luya\basicauth\Module;

$this->title = '';
?>

<div class="basicauth">
    <div class="basicauth-logo">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100px" viewBox="0 0 84 127.79">
            <defs>
                <style>.cls-1, .cls-2 {
                        fill-rule: evenodd;
                    }

                    .cls-11 {
                        fill: url(#GradientFill_1);
                    }

                    .cls-222 {
                        fill: url(#GradientFill_2);
                    }</style>
                <linearGradient id="GradientFill_1" x1="23.71" y1="101.05" x2="31.51" y2="8.83" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#41945d"></stop>
                    <stop offset="1" stop-color="#afca05"></stop>
                </linearGradient>
                <linearGradient id="GradientFill_2" x1="55.13" y1="88.31" x2="58.63" y2="57.18" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#f0aa00"></stop>
                    <stop offset="1" stop-color="#ef7c00"></stop>
                </linearGradient>
            </defs>
            <path class="cls-11" d="M36.71,61c-4.43-3.34-6.26-7.93-2-14.29A119.75,119.75,0,0,1,55.33,23.62c3.29-2.71,3.48-3.64.68-7.42L45.22,2.42C43-.38,42.86-.83,39,1.48,28.82,6.86,17.68,18.81,9.74,30.77-5.36,53.49-1.27,72,10.88,84.31a4.71,4.71,0,0,0,7-.57l19.4-19.4A2.3,2.3,0,0,0,36.71,61Z"></path>
            <path class="cls-222" d="M47.29,66.8c4.43,3.34,6.26,7.93,2,14.29a119.75,119.75,0,0,1-20.65,23.08c-3.29,2.71-3.48,3.64-.68,7.42l10.78,13.78c2.24,2.79,2.36,3.25,6.24.93C55.18,120.93,66.32,109,74.26,97c15.1-22.72,11-41.19-1.15-53.54a4.71,4.71,0,0,0-7,.57l-19.4,19.4A2.3,2.3,0,0,0,47.29,66.8Z"></path>
        </svg>
    </div>

    <p class="basicauth-lead"></p>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'basicauth-form']]); ?>
        <?= $form->field($model, 'input', [
            'template' => '{input}{label}',
            'labelOptions' => [ 'class' => 'basicauth-label' ]
        ])->passwordInput([
            'class' => 'basicauth-password',
            'autofocus' => true
        ])->label('Пароль'); ?>
        <div class="basicauth-buttons">
            <?= Html::submitButton('Вход', ['class' => 'basicauth-submit']); ?>
        </div>
    <?php $form::end(); ?>
</div>

<div class="basicauth-info">
    <p class="basicauth-info-title"><?= Yii::$app->siteTitle; ?></p>
    <p class="basicauth-info-text">
        <?php if (Yii::$app->request->isSecureConnection): ?>
            <span class="basicauth-ssl-icon" alt="<?= Module::t('ssl_info');?>" title="<?= Module::t('ssl_info');?>">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 1L3 5v6c0 5.55 3.84 10.74 9 12 5.16-1.26 9-6.45 9-12V5l-9-4zm-2 16l-4-4 1.41-1.41L10 14.17l6.59-6.59L18 9l-8 8z" fill="#28a745" /></svg>
            </span>
        <?php endif; ?>
        <span><?= Yii::$app->request->hostInfo; ?></span>
    </p>
</div>