<?php
/**
 * @var \luya\web\View $this
 */
?>

<script id="plugins-input-select" type="text/v-template">
    <div class="input-select">
        <template v-if="name && selected.length">
            <input v-if="multiple" type="hidden" :name="name.slice(0, name.length - 2)" value>
            <select :name="name" :multiple="multiple" style="display: none;">
                <option v-for="option in variants" :key="option.value" :value="option.value"
                        :selected="isSelected(option.value)">
                    {{ option.label }}
                </option>
            </select>
        </template>
        <div class="sitis-select">
            <ul v-if="!opened" class="sitis-select__dropdown uk-list">
                <li v-for="option in variants"
                    :key="option.value"
                    @click="select(option)"
                    :class="{'uk-text-success': isSelected(option.value), 'uk-text-muted': !isAvailable(option.value)}"
                ><input class="uk-checkbox" type="checkbox" :checked="isSelected(option.value)"> {{ option.label }}
                </li>
            </ul>
        </div>
    </div>
</script>