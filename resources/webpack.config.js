module.exports = {
    devServer: {
        host: 'dev.local'
    },
    entry: {
        main: './src/index.js',
        vendor: ['./src/less/theme.less'],
    },
};