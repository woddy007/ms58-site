<?php

namespace app\controllers;

use app\modules\user\models\User;
use Yii;

/**
 * Base Controller.
 *
 */
class BaseApiController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var bool
     */
    public $enableCors = true;

    /**
     * @var array
     */
    public $authOptional = ['login', 'options'];

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Expose-Headers' => ['X-Pagination-Total-Count, X-Pagination-Page-Count, X-Pagination-Current-Page, X-Pagination-Per-Page'],
                'Access-Control-Allow-Origin' => ['*'],
                'Access-Control-Allow-Headers' => ['origin, x-requested-with, content-type, Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization'],
                'Access-Control-Allow-Methods' => ['PUT, GET, POST, DELETE, OPTIONS'],
            ],
        ];

        return $behaviors;
    }


    /**
     * @param \yii\base\Action $action
     * @return bool|void
     */
    public function beforeAction($action)
    {
        if ($action->id != 'options' && Yii::$app->getRequest()->getMethod() == 'OPTIONS') {
            $this->runAction('options');
            Yii::$app->end();
        }

        return parent::beforeAction($action);
    }

}