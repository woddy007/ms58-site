<?php

namespace app\modules\instructions\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Instructions.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $instructions
 */
class Instructions extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%instructions}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-instructions-instructions';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'instructions' => Yii::t('app', 'Instructions'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instructions'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'instructions' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['instructions']],
            [['create', 'update'], ['instructions']],
            ['delete', false],
        ];
    }
}