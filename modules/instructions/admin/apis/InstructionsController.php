<?php

namespace app\modules\instructions\admin\apis;

/**
 * Instructions Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class InstructionsController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\instructions\models\Instructions';
}