<?php

namespace app\modules\instructions\admin;

/**
 * Instructions Admin Module.
 *
 * File has been created with `module/create` command. 
 * 
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
    public $apis = [
        'api-instructions-instructions' => 'app\modules\instructions\admin\apis\InstructionsController',
    ];
    
    public function getMenu()
    {
        return (new \luya\admin\components\AdminMenuBuilder($this))
            ->node('Помощь', 'extension')
                ->group('Инструкции')
                    ->itemApi('Инструкция пользователя', 'instructionsadmin/instructions/index', 'label', 'api-instructions-instructions');
    }
}