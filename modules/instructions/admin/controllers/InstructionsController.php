<?php

namespace app\modules\instructions\admin\controllers;

/**
 * Instructions Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class InstructionsController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\instructions\models\Instructions';

    public function actionIndex($inline = false, $relation = false, $arrayIndex = false, $modelClass = false, $modelSelection = false)
    {
        return $this->render('instructions');
    }
}