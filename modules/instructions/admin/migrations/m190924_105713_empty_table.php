<?php

use yii\db\Migration;

/**
 * Class m190924_105713_empty_table
 */
class m190924_105713_empty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('instructions', [
            'id' => $this->primaryKey(),
            'instructions' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('instructions');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190924_105713_empty_table cannot be reverted.\n";

        return false;
    }
    */
}
