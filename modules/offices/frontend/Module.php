<?php

namespace app\modules\offices\frontend;

/**
 * Offices Admin Module.
 *
 * File has been created with `module/create` command.
 *
 * @author
 * @since 1.0.0
 */
class Module extends \luya\base\Module
{
    public $urlRules = [
        ['pattern' => 'categories/<category:[a-zA-Z0-9\-]+>/', 'route' => 'services/services/index'],
        ['pattern' => 'categories/<category:[a-zA-Z0-9\-]+>/<service:[a-zA-Z0-9\-]+>/', 'route' => 'services/service/index'],
    ];
}
