<?php
namespace app\modules\services\frontend\controllers;

use app\modules\offices\models\Office;
use app\modules\services\models\Category;
use app\modules\services\models\Service;
use luya\web\Controller;
use yii\web\NotFoundHttpException;

class OfficesController extends Controller
{

    public $layout = '@app/views/layouts/main';


    public function actionIndex($category)
    {
        $offices = Office::find()->asArray()->all();

        return $this->render('index', [
            'offices' => $offices,
        ]);
    }


}
