<?php

use sitis\blocks\components\helpers\ImageHelper;
use app\modules\services\frontend\controllers\OfficeController;

use sitis\shop\core\helpers\BonusHelper as BonusHelperAlias;

$this->title = 'Услуга';
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['/categories']];
$this->params['breadcrumbs'][] = ['label' => $category['name'], 'url' => ['/categories/' . $category['slug']]];
$this->params['breadcrumbs'][] = $serviceData['name'];

?>

<div
        id="vue-page-service"
        class="tm-page-service"
>
    <div class="uk-container">

        <div class="tm-title"><?= $serviceData['name'] ?></div>
        <div class="tm-page-service__title-category"><?= $category['name'] ?></div>

        <div class="tm-page-service__block-info">
            <div class="tm-page-service__block-top-info-content">
                <div class="tm-page-service__image-content" data-uk-lightbox="animation: fade">
                    <a
                            href="<?= ImageHelper::getImage($serviceData['image']) ?>"
                            class="tm-page-service__image"
                    >
                        <img src="<?= ImageHelper::getImage($serviceData['image']) ?>" alt="<?= $serviceData['name'] ?>"
                             class="tm-page-service__image">
                    </a>
                </div>
                <div class="tm-page-service__information">
                    <div v-if="false" class="tm-page-service__tags">
                        <a
                                href="#"
                                class="tm-page-service__tag"
                        >
                            волосы короткие
                        </a>
                        <a
                                href="#"
                                class="tm-page-service__tag"
                        >
                            Женская стрижка
                        </a>
                    </div>
                    <div class="tm-page-service__info-service">
                        <div class="tm-card-service__info-item">
                            <div class="tm-card-service__info-item-icon tm-card-service__info-item-icon-green">
                                ₽
                            </div>
                            <div class="tm-card-service__info-item-value">
                                <?= $serviceData['price'] ?> ₽
                            </div>
                        </div>
                        <div class="tm-card-service__info-item">
                            <div class="tm-card-service__info-item-icon tm-card-service__info-item-icon-blue">
                                <span data-uk-icon="clock"></span>
                            </div>
                            <div class="tm-card-service__info-item-value">
                                <?= $serviceData['lead_time'] ?> мин
                            </div>
                        </div>
                    </div>
                    <?php if ($serviceData['description']): ?>
                        <div class="tm-page-service__description">
                            <?= $serviceData['description'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="tm-page-service__block-body-info-content">
                <div
                        v-if="subcategories.length > 0"
                        class="tm-card-service__subcategories"
                >
                    <div class="tm-page-service__additional-services-label">Основные детали</div>

                    <div class="tm-card-service__subcategories-content">
                        <div
                                v-for="(item, key) in subcategories"
                                class="tm-card-service__subcategory tm-application-processing__formitem-value"
                        >
                            <div class="tm-card-service__subcategory-title">{{item.subcategory.name}}</div>
                            <div
                                    :id="'subcategory-' + key"
                                    class="uk-select">
                                <span v-if="item.value">{{ item.value.name }}</span>
                                <span
                                        v-else
                                        class="tm-application-processing__formitem-value-placeholder"
                                >Выберите</span>
                            </div
                            >
                            <div
                                    class="tm-application-processing__formitem-value-list"
                                    data-uk-dropdown="mode: click; offset: 0; delay-hide: 0"
                            >
                                <div
                                        v-for="itemOptions in item.items"
                                        class="tm-application-processing__formitem-value-list-item"
                                        @click="() => { item.value = itemOptions; closeSelect(); recalculationPrice(); getHairDye() }"
                                >
                                    {{ itemOptions.name }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div
                        v-if="paints.length > 0"
                        class="tm-page-service__additional-services"
                >
                    <div class="tm-page-service__additional-services-label">Краски</div>
                    <div class="tm-page-service__options-list">
                        <label
                                v-for="(item, key) in paints"
                                :key="'paints-' + key"
                                class="tm-page-service__option"
                        >
                            <input
                                    v-model="paintEntry"
                                    :value="item.id"
                                    class="tm-page-service__option-checkbox uk-radio"
                                    type="radio"
                                    @change="recalculationPrice()"
                            >
                            <div class="tm-page-service__option-name">{{ item['name'] }}</div>
                            <div class="tm-page-service__option-price">{{ item['price'] }} ₽</div>
                        </label>
                    </div>
                </div>
                <div
                        v-if="options.length > 0"
                        class="tm-page-service__additional-services"
                >
                    <div class="tm-page-service__additional-services-label">Дополнительные услуги</div>
                    <div class="tm-page-service__options-list">
                        <label
                                v-for="(item, key) in options"
                                :key="'option-' + key"
                                class="tm-page-service__option"
                        >
                            <input
                                    v-model="item.entry"
                                    class="tm-page-service__option-checkbox uk-checkbox"
                                    type="checkbox"
                                    @change="recalculationPrice()"
                            >
                            <div class="tm-page-service__option-name">{{ item['name'] }}</div>
                            <div class="tm-page-service__option-price">{{ item['price'] }} ₽</div>
                            <div
                                    class="tm-page-service__option-info"
                                    :uk-tooltip="item.description"
                            >
                                <span data-uk-icon="question"></span>
                            </div>
                        </label>
                    </div>
                </div>
                <div
                        v-if="totalPrice"
                        class="tm-page-service__action"
                >
                    <div class="tm-page-service__price">
                        <div class="tm-page-service__price-label">Итого:</div>
                        <div class="tm-page-service__price-value">{{totalPrice}} ₽</div>
                        <div class="tm-page-service__time-value">{{totalTime}} минут</div>
                    </div>
                    <button
                            class="tm-page-service__button-order uk-button uk-button-secondary"
                            @click="_orderClick"
                    >ЗАКАЗАТЬ
                    </button>
                </div>
            </div>

            <transition v-if="showBlockApplications" name="fade">
                <div class="tm-page-service__block-applications tm-service-applications">
                    <div class="tm-service-applications__separator"></div>

                    <div id="section-application" class="tm-application-processing__content">
                        <div class="tm-application-processing__title">Заполните заявку</div>

                        <div class="tm-application-processing__selected-service tm-application-processing__caption">
                            <div>Вы выбрали: <span><?= $serviceData['name'] ?></span></div>
                        </div>

                        <div
                                v-if="getListEntryOptions().length > 0"
                                class="tm-application-processing__additional-services"
                        >
                            <div class="tm-application-processing__section-title">
                                Вы выбрали дополнительно
                            </div>
                            <div class="tm-application-processing__additional-services-list">
                                <div
                                        v-for="(item, key) in getListEntryOptions()"
                                        class="tm-application-processing__additional-service"
                                        @click="() => { item.entry = false; recalculationPrice() }"
                                >
                                    {{ item.name }}
                                    <div class="tm-application-processing__additional-service-price">{{ item.price }}
                                        ₽
                                    </div>
                                    <span data-uk-icon="close"></span>
                                </div>
                            </div>
                            <div class="tm-application-processing__additional-services-total-price">
                                Всего на сумму: <span>{{ totaSummAdditional }} ₽</span>
                            </div>
                        </div>

                        <div class="tm-service-applications__section-contant-information">
                            <div class="tm-application-processing__section-title">
                                Дополнительная информация
                            </div>

                            <div id="service-mobile" class="tm-service__section-phone tm-service-applications__contantinformation-line">
                                <div class="uk-flex uk-margin-small-bottom">
                                    <masked-input
                                            v-model="phone"
                                            :mask="['+', '7', ' ', '(', /[1-69]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, '-', /\d/, /\d/]"
                                            type="text"
                                            class="uk-input"
                                            placeholder="+7 (___) ___ __-__*"
                                    ></masked-input>
                                    <button
                                            class="uk-button uk-button-primary uk-button-small uk-text-nowrap"
                                            @click="sendPhone"
                                    >
                                        Получить код
                                    </button>
                                </div>
                                <transition v-if="phoneShowVerification" name="fade">
                                    <div class="uk-flex">
                                        <div v-if="checkNumber" class="tm-service-applications__contantinformation-line-check-icon">
                                            <span data-uk-icon="check"></span>
                                        </div>
                                        <masked-input
                                                :mask="[/[0-9]/,/\d/,/\d/,/\d/,/\d/,/\d/]"
                                                type="text"
                                                v-model="phoneVerificationCode"
                                                class="uk-input"
                                                placeholder="Код из СМС"
                                        ></masked-input>
                                        <button
                                                class="uk-button uk-button-primary uk-button-small"
                                                @click="checkCode"
                                        >
                                            Подтвердить
                                        </button>
                                    </div>
                                </transition>
                                <div v-if="textErrorPhone" class="tm-service-applications__contantinformation-line--error">
                                    {{ textErrorPhone }}
                                </div>
                            </div>
                        </div>

                        <div class="tm-application-processing__servicetime">
                            <div class="tm-application-processing__section-title">
                                Когда удобно встретиться?
                            </div>

                            <div class="tm-application-processing__caption">
                                Выберите желаемую дату и время, когда вы хотите воспользоваться услугой
                            </div>

                            <div class="tm-application-processing__caption-small">
                                с вами свяжется администратор для согласования деталей
                            </div>

                            <div class="tm-application-processing__servicetime-form">
                                <div class="tm-application-processing__formitem">
                                    <div class="tm-application-processing__formitem-label">Дата</div>
                                    <div class="tm-application-processing__formitem-value">
                                        <date-picker
                                                v-model="application.date"
                                                valueType="format"
                                                placeholder="Выберите"
                                                class="tm-application-processing__datepicker"
                                                format="DD.MM.YYYY"
                                                :lang="dateLang"
                                                :not-before="currentDate"
                                        />
                                    </div>
                                </div>

                                <div class="tm-application-processing__formitem">
                                    <div class="tm-application-processing__formitem-label">Время</div>
                                    <div class="tm-application-processing__formitem-value">
                                        <div class="uk-select">
                                            <span v-if="timepicker">{{ timepicker.label }}</span>
                                            <span v-else
                                                  class="tm-application-processing__formitem-value-placeholder">Выберите время</span>
                                        </div>
                                        <div class="tm-application-processing__formitem-value-list"
                                             uk-dropdown="mode: click; offset: 0; delay-hide: 0">
                                            <div
                                                    v-for="item in rangerTimepicker"
                                                    class="tm-application-processing__formitem-value-list-item"
                                                    @click="enterSelect( item, 'timepicker' )"
                                            >{{ item.label }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tm-application-processing__formitem uk-flex uk-flex-middle">
                                    <div class="tm-application-processing__formitem-label"></div>
                                    <div class="uk-text-italic tm-application-processing__formitem-value">
                                        Время выполнения</br>
                                        займет {{ totalTime }} минут
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tm-application-processing__addressblock">
                            <div class="tm-application-processing__section-title">
                                Где вам удобно встретиться?
                            </div>

                            <div v-if="false" class="tm-application-processing__radio">
                                <label>
                                    <input class="uk-radio" type="radio" value="2" v-model="place">
                                    <span>Нужен выезд мастера на дом</span>
                                </label>

                                <label>
                                    <input class="uk-radio" type="radio" value="1" v-model="place">
                                    <span>Поеду к мастеру</span>
                                </label>
                            </div>
                            <div class="tm-application-processing__addressblock-form">
                                <div id="application-form-address" v-if="place == 1"
                                     class="tm-application-processing__formitem">
                                    <div class="tm-application-processing__formitem-label">Район</div>
                                    <div class="tm-application-processing__formitem-value">
                                        <div class="uk-select">
                                            <span v-if="address">{{ address }}</span>
                                            <span v-else class="tm-application-processing__formitem-value-placeholder">Выберите район</span>
                                        </div>
                                        <div
                                                class="tm-application-processing__formitem-value-list"
                                                uk-dropdown="mode: click; offset: 0; delay-hide: 0"
                                        >
                                            <div
                                                    v-for="item in listArea"
                                                    class="tm-application-processing__formitem-value-list-item"
                                                    @click="enterSelect( item, 'address' )"
                                            >
                                                {{ item }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="application-form-address" v-else-if="place == 2"
                                     class="tm-application-processing__formitem">
                                    <div class="tm-application-processing__formitem-label">Адрес</div>
                                    <div class="tm-application-processing__formitem-value">
                                        <div
                                                ref="address-service"
                                                class="tm-address-complite"
                                                v-on:keyup.space="addressSpace"
                                                v-on:keyup.up="addressKeyup"
                                                v-on:keyup.down="addressDown"
                                                v-on:keyup.enter="addressEnter"
                                                v-on:keyup.esc="addressCloseFilter"
                                        >
                                            <input
                                                    v-model="address"
                                                    type="text"
                                                    placeholder="Введите адрес"
                                                    class="uk-input tm-address-complite__input"
                                                    v-on:input="inputAddress"
                                            >
                                            <div v-if="showDropDownAddress" class="tm-address-complite__container">
                                                <div
                                                        v-if="addressList.length > 0"
                                                >
                                                    <div
                                                            v-for="(item, key) in addressList"
                                                            class="tm-address-complite__item"
                                                            v-bind:class="{ 'tm-address-complite__item--active': key == activeAddressItemKey }"
                                                            @mouseover="activeAddressItemKey = key"
                                                            @click="addressClick(key)"
                                                    >
                                                        {{ item.value }}
                                                    </div>
                                                </div>
                                                <div
                                                        v-else
                                                        class="tm-address-complite__notitem"
                                                >
                                                    Нечего не найдено
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tm-application-processing__adding-images">
                            <div class="tm-application-processing__section-title">
                                Добавьте фото по задаче
                            </div>

                            <div class="tm-application-processing__addressblock-form">
                                <div
                                        v-if="listImages.length == 0"
                                        id="service-image-upload"
                                        class="tm-application-processing__image-uploader uk-placeholder uk-text-center"
                                        data-uk-form-custom
                                >
                                    <div>
                                        <input
                                                type="file"
                                                multiple
                                                name="images[]"
                                                @change="changeImageUploader"
                                        >
                                        <span class="uk-link">Загрузите фотографии</span>
                                    </div>
                                    <div>
                                        <span uk-icon="icon: cloud-upload"></span>
                                        <span class="uk-text-middle">или перетащите прямо сюда</span>
                                    </div>
                                </div>
                                <div v-else class="tm-application-processing__images-list">
                                    <div
                                            v-for="(item, key) in listImages"
                                            class="tm-application-processing__images-item"
                                    >
                                        <img
                                                :src="getImageToBase64(item, key)"
                                                :ref="'upload-image-' + key"
                                                class="tm-application-processing__images-item-image"
                                        >
                                        <div
                                                class="tm-application-processing__images-item-delete"
                                                @click="deleteImage(key)"
                                        >
                                            <span data-uk-icon="close"></span>
                                        </div>
                                    </div>

                                    <div
                                            v-if="listImages.length <= 4"
                                            class="tm-application-processing__images-item tm-application-processing__images-item--added"
                                            data-uk-form-custom
                                    >
                                        <input
                                                type="file"
                                                multiple
                                                name="images[]"
                                                @change="changeImageUploader"
                                        >
                                        <span>Добавить</span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tm-application-processing__addressblock-form">
                            <div class="tm-application-processing__formitem">
                                <div class="tm-application-processing__formitem-label">Опишите пожелания и детали</div>
                                <div class="tm-application-processing__formitem-value">
                            <textarea
                                    v-model="wishesdetails"
                                    rows="2"
                                    class="uk-input"
                                    placeholder="Опишите пожелания и детали, окрашивали ли вы волосы, какие ожидания от прически"
                            ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tm-service-applications__separator"></div>

                    <div v-if="false" class="tm-service-applications__order-information">
                        <div class="tm-page-service__price tm-service-applications__order-information-price">
                            <div class="tm-page-service__price-label">Итого:</div>
                            <div class="tm-page-service__price-value">{{totalPrice}} ₽</div>
                        </div>
                        <div class="tm-service-applications__order-information-bonuses">
                            У меня <span>100</span> баллов на счету
                        </div>

                        <div class="tm-service-applications__order-information-action">
                            <div class="tm-application-processing__radio">
                                <label>
                                    <input class="uk-checkbox" type="checkbox" value="true" v-model="writeBonuses">
                                    <span>Расплатиться 89 баллами</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div v-if="false" class="tm-service-applications__separator"></div>

                    <div class="tm-service-applications__section-contant-information">
                        <div class="tm-application-processing__section-title">
                            Дополнительная информация
                        </div>

                        <div class="tm-service-applications__contantinformation-line">
                            <input
                                    v-model="email"
                                    type="text"
                                    class="uk-input"
                                    placeholder="E-mail"
                            >
                            <span>Этот адрес электронной почты будет использоваться для получения электронных чеков.</span>
                        </div>
                        <div class="tm-service-applications__contantinformation-line">
                            <input
                                    v-model="name"
                                    type="text"
                                    class="uk-input"
                                    placeholder="Имя"
                            >
                        </div>
                    </div>

                    <div class="tm-service-applications__separator"></div>

                    <div class="tm-service__type-pay">
                        <div class="tm-service__type-pay-label">Оплата</div>
                        <ul class="tm-service__type-pay-items">
                            <li>
                                <label>
                                    <input
                                            v-model="payment"
                                            type="radio"
                                            name="payment"
                                            value="1"
                                            class="uk-radio"
                                    >
                                    <span>Наличными</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input
                                            v-model="payment"
                                            type="radio"
                                            name="payment"
                                            value="2"
                                            class="uk-radio"
                                    >
                                    <span>Банковской картой</span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="tm-page-service__price tm-service-applications__order-information-price">
                        <div class="tm-page-service__price-label">Итого:</div>
                        <div class="tm-page-service__price-value">{{totalPrice}} ₽</div>
                    </div>

                    <button
                            class="uk-button uk-button-secondary uk-width-1-1 uk-margin-small-bottom"
                            @click="sendRequest()"
                    >
                        Отправить заявку
                    </button>

                    <div id="form-privace">
                        <label>
                            <input
                                    v-model="privace"
                                    class="uk-checkbox"
                                    type="checkbox"
                            >
                            <span class="tm-page-service__publichnaya-oferta-button" @click="openModalOferta()">
                                Согласен на обработку персональных данных
                            </span>
                        </label>
                    </div>
                </div>
            </transition>
        </div>

        <div class="tm-page-service__title tm-title">Другие услуги</div>
        <div class="tm-page-service__list-other">
            <div
                    class="tm-page-services__list-services tm-list-services"
                    data-uk-height-match="target: > div > .tm-card-service > .tm-card-service__content > .tm-card-service__name"
            >
                <?php foreach ($services as $key => $item): ?>
                    <div>
                        <a href="/categories/<?= $category['slug'] ?>/<?= $item['slug'] ?>" class="tm-card-service">
                            <div class="tm-card-service__image">
                                <img src="<?= ImageHelper::getImage($item['image']) ?>" alt="">
                            </div>
                            <div class="tm-card-service__content">
                                <div class="tm-card-service__name"><?= $item['name'] ?></div>
                                <div class="tm-card-service__info">
                                    <div class="tm-card-service__info-item">
                                        <div class="tm-card-service__info-item-icon tm-card-service__info-item-icon-green">
                                            ₽
                                        </div>
                                        <div class="tm-card-service__info-item-value">
                                            <?= $item['price'] ?>
                                        </div>
                                    </div>
                                    <div class="tm-card-service__info-item">
                                        <div class="tm-card-service__info-item-icon tm-card-service__info-item-icon-blue">
                                            <span data-uk-icon="clock"></span>
                                        </div>
                                        <div class="tm-card-service__info-item-value">
                                            <?= $item['lead_time'] ?> мин
                                        </div>
                                    </div>
                                </div>
                                <button class="tm-card-service__button uk-button uk-button-secondary">Подробнее</button>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div id="notification-application" data-uk-modal>
        <div class="uk-modal-dialog tm-authorization-application tm-notification-application">
            <div class="tm-authorization-application__content">
                <div class="tm-authorization-application__left">
                    <img src="/static/image/modal-application-image.png" alt="">
                </div>
                <div class="tm-authorization-application__right">
                    <div class="tm-authorization-application__title">Успешно</div>
                    <div class="tm-authorization-application__text">Ваша заявка отправлена на обработку</div>
                    <a href="/" class="uk-button uk-button-primary uk-width-1-1">На главную</a>
                    <a
                            :href="dopServises.url"
                            class="uk-button uk-button-primary uk-width-1-1"
                            v-if="dopServises.url"
                    >
                        Дополнительные услуги
                    </a>
                    <div
                            v-if="dopServises.services.length > 0"
                         class="tm-authorization-application__list-additionalservises"
                    >
                        <a
                                v-for="(item) in dopServises.services"
                                :href="item.url"
                        >{{ item.label }}</a>
                    </div>
                </div>
            </div>
            <button class="uk-modal-close-outside" type="button" data-uk-close></button>
        </div>
    </div>

    <div id="publichnaya-oferta" data-uk-modal>
        <div class="uk-modal-dialog tm-publichnaya-oferta">
            <div class="tm-publichnaya-oferta__content">

            </div>
            <button class="uk-modal-close-outside" type="button" data-uk-close></button>
        </div>
    </div>

</div>

<input id="vue-page-service-options" type="text" value="<?= $idServises ?>" hidden>
