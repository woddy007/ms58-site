<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Video Connect Office.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $office_id
 * @property integer $videocam_id
 */
class VideoConnectOffice extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video_connect_office}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-videoconnectoffice';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'office_id' => Yii::t('app', 'Office ID'),
            'videocam_id' => Yii::t('app', 'Videocam ID'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id', 'videocam_id'], 'required'],
            [['office_id', 'videocam_id'], 'integer'],
            [['office_id', 'videocam_id'], 'unique', 'targetAttribute' => ['office_id', 'videocam_id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['videocam_id'], 'exist', 'skipOnError' => true, 'targetClass' => Videocams::className(), 'targetAttribute' => ['videocam_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'office_id' => 'number',
            'videocam_id' => 'number'
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['office_id']],
            ['delete', false],
        ];
    }
}