<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Videocams.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $name
 * @property integer $image
 * @property string $link
 */
class Videocams extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%videocams}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-videocams';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'image' => Yii::t('app', 'Изображение'),
            'link' => Yii::t('app', 'Ссылка на поток'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'integer'],
            [['name', 'link', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'name' => 'text',
            'image' => 'image',
            'link' => 'text',
            'description' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['name', 'description', 'image', 'link']],
            [['create', 'update'], ['name', 'description', 'image', 'link']],
            ['delete', true],
        ];
    }
    public function fields()
    {
        return array_merge(parent::fields(), [
            'image_path' => function() { return self::getImagesrc();},
        ]);
    }
    public function getImagesrc()
    {
        if ($this->image && $this->image > 0){
            return Yii::$app->storage->getImage($this->image)->sourceAbsolute;
        }
    }
}