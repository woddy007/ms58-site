<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Cards.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $number
 * @property string $emploer
 * @property integer $office_id
 */
class Cards extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cards}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-cards';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'Номер'),
            'emploer' => Yii::t('app', 'Сотрудник'),
            'office_id' => Yii::t('app', 'Офис'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id'], 'integer'],
            [['number', 'emploer'], 'string', 'max' => 255],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'number' => 'text',
            'emploer' => 'text',
            'office_id' => [
                'selectModel',
                'modelClass' => Office::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->company;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['number', 'emploer', 'office_id']],
            [['create', 'update'], ['number', 'emploer', 'office_id']],
            ['delete', true],
        ];
    }
}