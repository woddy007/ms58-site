<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;


/**
 * Skud.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $datetime
 * @property string $employee
 */
class Skud extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%skud}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-skud';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'office_id' => Yii::t('app', 'Офис'),
            'datetime' => Yii::t('app', 'Дата и время'),
            'employee' => Yii::t('app', 'Сотрудник'),
            'status' => Yii::t('app', 'Вход/Выход'),
            'event_id' => Yii::t('app', 'ID события'),
            'object' => Yii::t('app', 'Объект'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'office_id', 'event_id'], 'integer'],
            [['event_id'], 'unique'],
            [['employee', 'object', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'office_id' => [
                'selectModel',
                'modelClass' => Office::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->company;
                },
            ],
            'datetime' => 'datetime',
            'employee' => 'text',
            'status' => 'text',
            'object' => 'text',
            'event_id' => 'number'
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['office_id', 'object', 'datetime', 'employee', 'status', 'event_id']],
//            [['create', 'update'], ['office_id', 'datetime', 'employee', 'status']],
            ['delete', true],
        ];
    }
}