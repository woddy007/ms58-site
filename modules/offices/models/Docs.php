<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Docs.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $date
 * @property string $name
 * @property string $file
 */
class Docs extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%docs}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-docs';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'office_id' => Yii::t('app', 'Офис'),
            'date' => Yii::t('app', 'Дата'),
            'name' => Yii::t('app', 'Наименование'),
            'file' => Yii::t('app', 'Файл'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'name', 'file', 'office_id'], 'required'],
            [['date', 'office_id', 'file'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'office_id' => [
                'selectModel',
                'modelClass' => Office::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->company;
                },
            ],
            'date' => 'date',
            'name' => 'text',
            'file' => 'file',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['office_id', 'date', 'name', 'file']],
            [['create', 'update'], ['office_id', 'date', 'name', 'file']],
            ['delete', true],
        ];
    }
}