<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\modules\offices\models\Pribor;
/**
 * Uchet.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $date
 * @property string $type
 * @property string $indication
 */
class Uchet extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%uchet}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-uchet';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'office_id' => Yii::t('app', 'Офис'),
            'created_at' => Yii::t('app', 'Дата снятия'),
            'type' => Yii::t('app', 'Счетчик'),
            'indication' => Yii::t('app', 'Показания'),
        ];
    }
    public function behaviors()

    {

        return [


            'timestamp' => [

                'class' => 'yii\behaviors\TimestampBehavior',

                'attributes' => [

                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],

                    ActiveRecord::EVENT_BEFORE_UPDATE => false,

                ],

            ],

        ];

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'indication', 'office_id'], 'required'],
            [['created_at', 'office_id'], 'integer'],
            [['type', 'indication'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'office_id' => [
                'selectModel',
                'modelClass' => Office::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->company;
                },
            ],
            'created_at' => 'date',
            'type' => 'text',
            'indication' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['office_id', 'created_at', 'type', 'indication']],
            [['create', 'update'], ['office_id', 'type', 'indication']],
            ['delete', true],
        ];
    }
//    public function extraFields()
//    {
//        return [
//            'typeTitle' => function($model){
//            return Pribor::getType()[$model->type]; },
//        ];
//    }
}