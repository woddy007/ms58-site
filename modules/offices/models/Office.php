<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use app\modules\offices\models\Pribor;
use luya\admin\ngrest\plugins\CheckboxRelationActiveQuery;
use yii\db\ActiveQuery;

/**
 * Office.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $name
 */
class Office extends NgRestModel
{
   public $cams =[];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%office}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-office';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'owner' => Yii::t('app', 'Собственник'),
            'company' => Yii::t('app', 'Компания'),
            'pribor' => Yii::t('app', 'Приборы учета'),
            'cams' => Yii::t('app', 'Камеры'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['owner', 'pribor'], 'integer'],
            [['cams'], 'safe'],
            [['name', 'company'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'name' => 'text',
            'owner' => [
                'selectModel',
                'modelClass' => \app\modules\users\models\Owner::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->middlename . ' ' . $model->lastname;
                },
            ],
            'company' => 'text',
            'pribor' => [
                'selectModel',
                'modelClass' => Pribor::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->type . ' № ' . $model->number;
                },
            ]
        ];
    }
    public function ngRestExtraAttributeTypes()
    {

        return [
            'cams' => [
                'class' => CheckboxRelationActiveQuery::class,
                'query' => $this->getCams(),
                'labelField' => ['name'],
            ],
        ];
    }
    public function getCams(): ActiveQuery
    {
        return $this->hasMany(Videocams::class, ['id' => 'videocam_id'])->viaTable('video_connect_office', ['office_id' => 'id']);
    }
    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['name', 'owner', 'company', 'pribor']],
            [['update'], ['owner', 'company', 'pribor', 'cams']],
            ['delete', false],
        ];
    }

}