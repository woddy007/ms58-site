<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Pribor.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $number
 * @property string $type
 */
class Pribor extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pribor}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-pribor';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'Номер'),
            'type' => Yii::t('app', 'Тип'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['type'], 'integer'],
            [['number'], 'unique'],
            [['number'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'number' => 'text',
            'type' => ['selectArray', 'data' => self::getType()],
        ];
    }
    public function extraFields()
    {
        return [
            'typeTitle' => function($model){ 
            return self::getType()[$model->type]; },
        ];
    }
    public static function getType()
    {
        return [1 => 'Электро', 2 => 'ХВС', 3 => 'ГВС', 4 => 'Тепло'];
    }
    public function fields()
    {
        return array_merge(parent::fields(), [
            'typename' => function($model) { return self::getType()[$model->type];},
        ]);
    }
    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['number', 'type']],
            [['create', 'update'], ['number', 'type']],
            ['delete', true],
        ];
    }
}