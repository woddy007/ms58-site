<?php

namespace app\modules\offices\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Officebills.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $date
 * @property string $name
 * @property string $file
 * @property integer $status
 */
class Officebills extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%officebills}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-offices-officebills';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'office_id' => Yii::t('app', 'Офис'),
            'date' => Yii::t('app', 'Дата'),
            'name' => Yii::t('app', 'Наименование'),
            'file' => Yii::t('app', 'Файл счета'),
            'status' => Yii::t('app', 'Оплачен'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'name', 'file', 'status', 'office_id'], 'required'],
            [['status', 'date', 'file', 'office_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'office_id' => [
                'selectModel',
                'modelClass' => Office::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->company;
                },
            ],
            'date' => 'date',
            'name' => 'text',
            'file' => 'file',
            'status' => 'toggleStatus',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['office_id', 'date', 'name', 'file', 'status']],
            [['create', 'update'], ['office_id', 'date', 'name', 'file', 'status']],
            ['delete', true],
        ];
    }
}