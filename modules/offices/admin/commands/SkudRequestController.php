<?php

namespace app\modules\offices\admin\commands;

use app\modules\offices\models\Cards;
use app\modules\offices\models\Office;
use app\modules\offices\models\Skud;
use luya\console\Command;
use sitis\guardsaasreport\Guardsaasreportrequest;
use yii\web\NotFoundHttpException;


class SkudRequestController extends Command
{
    public function actionReport()
    {
        $dateTo = date('Y-m-d');
        $dateFrom = date("Y-m-d", mktime(date('h')-1, 0, 0, date('m'), date('d'), date('Y')));
        $offices = Office::find()->all();
        $report = Guardsaasreportrequest::reportrequest($dateFrom,$dateTo);
        $events = $report->items;
        foreach ($events as $event) {
            if ($event->eventid == 16 || $event->eventid == 17) {
                $datetime = strtotime($event->time);
                $office_number = (int) filter_var($event->object, FILTER_SANITIZE_NUMBER_INT);
                if ($office_number === 0 || $office_number === 58) {
                    continue;
                }
                foreach ($offices as $office) {
                    $office_numb = (int) filter_var($office->name, FILTER_SANITIZE_NUMBER_INT);
                    $office_numb = strval($office_numb);
                    $office_numb = substr($office_numb, 2);
                    $office_numb = intval($office_numb);
                    if ($office_numb == $office_number) {
                        $office_id = $office->id;
                        break;
                    }
                }
                $skud = new Skud([
                    'office_id' => $office_id,
                    'datetime' => $datetime,
                    'employee' => $event->cardCode,
                    'status' => $event->direction,
                    'event_id' => $event->id,
                    'object' => $event->object,
                ]);
                if (!$skud->save()) {
                    continue;
                }
            } else {
                continue;
            }
        }
        return 'Save complete';
    }
}
