<?php

namespace app\modules\offices\admin;

/**
 * Offices Admin Module.
 *
 * File has been created with `module/create` command. 
 * 
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
	    'api-offices-office' => 'app\modules\offices\admin\apis\OfficeController',
	    'api-offices-officebills' => 'app\modules\offices\admin\apis\OfficebillsController',
	    'api-offices-uchet' => 'app\modules\offices\admin\apis\UchetController',
	    'api-offices-docs' => 'app\modules\offices\admin\apis\DocsController',
		'api-offices-skud' => 'app\modules\offices\admin\apis\SkudController',
		'api-offices-pribor' => 'app\modules\offices\admin\apis\PriborController',
        'api-offices-videocams' => 'app\modules\offices\admin\apis\VideocamsController',
        'api-offices-help' => 'app\modules\offices\admin\apis\HelpController',
        'api-offices-cards' => 'app\modules\offices\admin\apis\CardsController',
    ];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Контроль офисов', 'account_balance')
	            ->group('Разделы')
					->itemApi('Офисы', 'officesadmin/office/index', 'store', 'api-offices-office')
					->itemApi('Приборы учета', 'officesadmin/pribor/index', 'av_timer', 'api-offices-pribor')
					->itemApi('Показания', 'officesadmin/uchet/index', 'offline_bolt', 'api-offices-uchet')
					->itemApi('Счета', 'officesadmin/officebills/index', 'monetization_on', 'api-offices-officebills')
	                ->itemApi('Документы', 'officesadmin/docs/index', 'description', 'api-offices-docs')
	                ->itemApi('СКУД', 'officesadmin/skud/index', 'vpn_key', 'api-offices-skud')
                    ->itemApi('Карты доступа', 'officesadmin/cards/index', 'credit_card', 'api-offices-cards')
                    ->itemApi('Видеокамеры', 'officesadmin/videocams/index', 'videocam    ', 'api-offices-videocams')
                    ->itemApi('Помощь', 'officesadmin/help/index', 'help_outline', 'api-offices-help');
	}

	public function extendPermissionRoutes()
    {
        return [
			['route' => 'officesadmin/office/download', 'alias' => 'download'],
			['route' => 'officesadmin/office/getoffices', 'alias' => 'getoffices'],
			['route' => 'officesadmin/pribor/getpribor', 'alias' => 'getpribor'],
        ];
    }

}