<?php

use yii\db\Migration;

/**
 * Class m190927_125653_add_column_pribor_number
 */
class m190927_125653_add_column_pribor_number extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('officebills', 'pribor_number', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('officebills', 'pribor_number');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_125653_add_column_pribor_number cannot be reverted.\n";

        return false;
    }
    */
}
