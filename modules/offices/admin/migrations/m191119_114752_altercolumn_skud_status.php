<?php

use yii\db\Migration;

/**
 * Class m191119_114752_altercolumn_skud_status
 */
class m191119_114752_altercolumn_skud_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('skud', 'status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191119_114752_altercolumn_skud_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191119_114752_altercolumn_skud_status cannot be reverted.\n";

        return false;
    }
    */
}
