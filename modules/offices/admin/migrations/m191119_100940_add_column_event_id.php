<?php

use yii\db\Migration;

/**
 * Class m191119_100940_add_column_event_id
 */
class m191119_100940_add_column_event_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%skud}}', 'event_id', $this->integer());
        $this->addColumn('{{%skud}}', 'object', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%skud}}', 'event_id');
        $this->dropColumn('{{%skud}}', 'object');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191119_100940_add_column_event_id cannot be reverted.\n";

        return false;
    }
    */
}
