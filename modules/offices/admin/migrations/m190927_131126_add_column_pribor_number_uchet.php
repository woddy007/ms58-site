<?php

use yii\db\Migration;

/**
 * Class m190927_131126_add_column_pribor_number_uchet
 */
class m190927_131126_add_column_pribor_number_uchet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('uchet', 'pribor_number', $this->string());
        $this->dropColumn('officebills', 'pribor_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('uchet', 'pribor_number');
        $this->addColumn('officebills', 'pribor_number', $this->string());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_131126_add_column_pribor_number_uchet cannot be reverted.\n";

        return false;
    }
    */
}
