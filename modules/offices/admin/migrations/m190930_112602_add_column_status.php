<?php

use yii\db\Migration;

/**
 * Class m190930_112602_add_column_status
 */
class m190930_112602_add_column_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('skud', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('skud', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190930_112602_add_column_status cannot be reverted.\n";

        return false;
    }
    */
}
