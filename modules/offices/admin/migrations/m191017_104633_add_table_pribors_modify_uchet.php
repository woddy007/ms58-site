<?php

use yii\db\Migration;

/**
 * Class m191017_104633_add_table_pribors_modify_uchet
 */
class m191017_104633_add_table_pribors_modify_uchet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('uchet', 'date');
        $this->addColumn('uchet', 'created_at', $this->integer());
        $this->createTable('{{%pribor}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string()->notNull(),
            'type' => $this->string(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('uchet', 'created_at');
        $this->addColumn('uchet', 'date', $this->integer());
        $this->dropTable('{{%pribor}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_104633_add_table_pribors_modify_uchet cannot be reverted.\n";

        return false;
    }
    */
}
