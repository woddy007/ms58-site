<?php

use yii\db\Migration;

/**
 * Class m191008_102723_add_column_company
 */
class m191008_102723_add_column_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%office}}', 'company', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%office}}', 'company');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191008_102723_add_column_company cannot be reverted.\n";

        return false;
    }
    */
}
