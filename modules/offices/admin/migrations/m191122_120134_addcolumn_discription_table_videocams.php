<?php

use yii\db\Migration;

/**
 * Class m191122_120134_addcolumn_discription_table_videocams
 */
class m191122_120134_addcolumn_discription_table_videocams extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%videocams}}', 'description', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%videocams}}', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191122_120134_addcolumn_discription_table_videocams cannot be reverted.\n";

        return false;
    }
    */
}
