<?php

use yii\db\Migration;

/**
 * Class m191118_144443_add_tables_videocams_cards_help
 */
class m191118_144443_add_tables_videocams_cards_help extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%videocams}}',[
           'id' => $this->primaryKey(),
           'name' => $this->string(),
           'image' => $this->integer(),
            'link' => $this->string(),
        ]);
        $this->createTable('{{%video_connect_office}}', [
            'office_id' => $this->integer()->notNull(),
            'videocam_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('{{%pk-video_connect_office}}', '{{%video_connect_office}}', ['office_id', 'videocam_id']);
        $this->createIndex(
            '{{%idx-video_connect_office-office_id}}',
            '{{%video_connect_office}}',
            'office_id'
        );

        $this->addForeignKey(
            '{{%fk-video_connect_office-office_id}}',
            '{{%video_connect_office}}',
            'office_id',
            '{{%office}}',
            'id',
            'RESTRICT'
        );
        $this->createIndex(
            '{{%idx-video_connect_office-videocam_id}}',
            '{{%video_connect_office}}',
            'videocam_id'
        );

        $this->addForeignKey(
            '{{%fk-video_connect_office-videocam_id}}',
            '{{%video_connect_office}}',
            'videocam_id',
            '{{%videocams}}',
            'id',
            'RESTRICT'
        );
        $this->createTable('{{%cards}}', [
           'id' => $this->primaryKey(),
           'number' => $this->string(),
           'emploer' => $this->string(),
           'office_id' => $this->integer(),
        ]);
        $this->createIndex(
            '{{%idx-cards-office_id}}',
            '{{%cards}}',
            'office_id'
        );

        $this->addForeignKey(
            '{{%fk-cards-office_id}}',
            '{{%cards}}',
            'office_id',
            '{{%office}}',
            'id',
            'RESTRICT'
        );
        $this->createTable('{{%help}}', [
           'id' => $this->primaryKey(),
            'name' => $this->string(),
            'text' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addColumn('{{office}}', 'cams', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191118_144443_add_tables_videocams_cards_help cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191118_144443_add_tables_videocams_cards_help cannot be reverted.\n";

        return false;
    }
    */
}
