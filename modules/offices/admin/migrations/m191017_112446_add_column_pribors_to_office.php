<?php

use yii\db\Migration;

/**
 * Class m191017_112446_add_column_pribors_to_office
 */
class m191017_112446_add_column_pribors_to_office extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('office', 'pribor', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('office', 'pribor');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_112446_add_column_pribors_to_office cannot be reverted.\n";

        return false;
    }
    */
}
