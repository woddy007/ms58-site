<?php

use yii\db\Migration;

/**
 * Class m190902_143236_offices
 */
class m190902_143236_offices extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $this->createTable('office', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'owner' => $this->string(),
        ]);
        $this->createTable('officebills', [
            'id' => $this->primaryKey(),
            'office_id' => $this->string(),
            'date' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'file' => $this->string()->notNull(),
            'status' => $this->integer(),
        ]);
        $this->createTable('uchet', [
            'id' => $this->primaryKey(),
            'office_id' => $this->string(),
            'date' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'indication' => $this->string()->notNull(),
        ]);
        $this->createTable('docs', [
            'id' => $this->primaryKey(),
            'office_id' => $this->string(),
            'date' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'file' => $this->string()->notNull(),
        ]);
        $this->createTable('skud', [
            'id' => $this->primaryKey(),
            'office_id' => $this->string(),
            'datetime' => $this->integer()->notNull(),
            'employee' => $this->string()->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('office');
        $this->dropTable('officebills');
        $this->dropTable('uchet');
        $this->dropTable('docs');
        $this->dropTable('skud');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190902_143236_offices cannot be reverted.\n";

        return false;
    }
    */
}
