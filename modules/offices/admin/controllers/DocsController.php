<?php

namespace app\modules\offices\admin\controllers;

/**
 * Docs Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class DocsController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Docs';
}