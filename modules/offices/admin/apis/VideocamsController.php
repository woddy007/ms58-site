<?php

namespace app\modules\offices\admin\apis;

use app\controllers\BaseApiController;
use app\modules\offices\models\Videocams;
use app\modules\offices\models\VideoConnectOffice;

/**
 * Videocams Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class VideocamsController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Videocams';

    public $authOptional = ['getcams', 'options'];

    public function actionGetcams($office_id)
    {
        $cams = [];
        $officecams = VideoConnectOffice::find()->where(['office_id' => $office_id])->all();
        foreach ($officecams as $cam){
            $cams [] = $cam->videocam_id;
        }
        return Videocams::find()->where(['in', 'id', $cams])->all();
    }
}