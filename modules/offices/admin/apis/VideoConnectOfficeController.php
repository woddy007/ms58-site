<?php

namespace app\modules\offices\admin\apis;

/**
 * Video Connect Office Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class VideoConnectOfficeController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\VideoConnectOffice';
}