<?php

namespace app\modules\offices\admin\apis;

use app\controllers\BaseApiController;
use yii\base\Action;
use app\modules\offices\models\Docs;
/**
 * Docs Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class DocsController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Docs';

    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex($office_id)
    {
        $docs = Docs::find()->where(['office_id' => $office_id])->all();
        return $docs;
    }

}