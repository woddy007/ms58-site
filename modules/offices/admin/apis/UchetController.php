<?php

namespace app\modules\offices\admin\apis;

use app\controllers\BaseApiController;
use app\modules\offices\models\Uchet;
use yii\base\Action;
use yii\data\ActiveDataProvider;
/**
 * Uchet Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class UchetController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Uchet';

    public $authOptional = ['test', 'searchbydaterange', 'options'];
    
    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex($off)
    {
        
        return new ActiveDataProvider([
            'query' => Uchet::find()->where(['office_id' => $off]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    public function actionSearchbydaterange ($date_from, $date_to, $off)
    {
        $date_from = $date_from . ' 01:00';
        $date_to = $date_to . ' 23:00';
        $date_from = strtotime($date_from);
        $date_to = strtotime($date_to);
        return new ActiveDataProvider([
            'query' => Uchet::find()->andWhere(['office_id' => $off])->andWhere(['>=', 'created_at', $date_from])->andWhere(['<=', 'created_at', $date_to]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    public function actionTest($off)

    {
        return Uchet::find()->andWhere(['office_id' => $off])->one();
    }

}