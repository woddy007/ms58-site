<?php

namespace app\modules\offices\admin\apis;

use app\modules\offices\models\Office;
use app\controllers\BaseApiController;
use app\modules\offices\models\Pribor;
use app\modules\offices\models\Uchet;
use Yii;
/**
 * Pribor Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class PriborController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Pribor';

    public $authOptional = ['takeindication', 'getpribor', 'options'];

    // public function actionPermissions()
    // {
    //     return ['takeindication' => \luya\admin\components\Auth::CAN_VIEW,
    //             'getpribors' => \luya\admin\components\Auth::CAN_VIEW,
    //     ];
    // }

    public function actionGetpribor($office_id)
    {
        $data =[];
        $pribor_id = Office::find()->where(['id' => $office_id])->one()->pribor;
        if ($pribor_id == ''){
            return 'Нет привязанных приборов';
        }
        $pribor = Pribor::find()->where(['id' => $pribor_id])->one();
        $data[] = [
        'id' => $pribor->id,
        'name' => Pribor::getType()[$pribor->type] . ' №' . $pribor->number,
        ];
        return $data;

    }

    public function actionTakeindication() 
    {
       $data = Yii::$app->request->getBodyParams();
       $pribor_id = $data['pribor_id'];
       $indication = $data['indication'];
       $office_id = Office::find()->where(['pribor' => $pribor_id])->one()->id;
       $type = Pribor::find()->where(['id' => $pribor_id])->one();
       $type_id=intval($type->type);
       $type_name = Pribor::getType()[$type_id] . ' № ' . $type->number;
       $uchet = new Uchet([
            'office_id' => $office_id,
            'type' => $type_name,
            'indication' => $indication,
       ]);
       if ($uchet->save()) {
            return 'All save';
       }
      return $uchet;

    }
}