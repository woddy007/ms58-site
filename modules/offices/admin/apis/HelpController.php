<?php

namespace app\modules\offices\admin\apis;

use app\controllers\BaseApiController;

/**
 * Help Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class HelpController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Help';
}