<?php

namespace app\modules\offices\admin\apis;

use app\controllers\BaseApiController;
use app\modules\offices\models\Office;
use app\modules\offices\models\Skud;
use sitis\guardsaasreport\Guardsaasreportrequest;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;


/**
 * Skud Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class SkudController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Skud';
    public $authOptional = ['test', 'report', 'options'];

    public function actionReport($datefrom, $dateto, $office_id)
    {
        $datefr = strtotime($datefrom);
        $datet = strtotime($dateto);
       return new ActiveDataProvider([
           'query' => Skud::find()->andWhere(['office_id' => $office_id])->andWhere(['>=', 'datetime', $datefr])->andWhere(['<=', 'datetime', $datet])->orderBy(['datetime'=> SORT_DESC]),
           'pagination' => [
               'pageSize' => 20,
           ],
       ]);
    }
    public function actionTest()
    {
        $dateTo = date('Y-m-d');
        $dateFrom = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') - 14, date('Y')));
        $offices = Office::find()->all();
        $report = Guardsaasreportrequest::reportrequest($dateFrom,$dateTo);
        $events = $report->items;
        foreach ($events as $event) {
            if ($event->eventid == 16 || $event->eventid == 17) {
                $datetime = strtotime($event->time);
                $office_number = (int) filter_var($event->object, FILTER_SANITIZE_NUMBER_INT);
                if ($office_number === 0 || $office_number === 58) {
                    continue;
                }
                foreach ($offices as $office) {
                    $office_numb = (int) filter_var($office->name, FILTER_SANITIZE_NUMBER_INT);
                    $office_numb = strval($office_numb);
                    $office_numb = substr($office_numb, 2);
                    $office_numb = intval($office_numb);
                    if ($office_numb == $office_number) {
                        $office_id = $office->id;
                        break;
                    }
                }
                $skud = new Skud([
                    'office_id' => $office_id,
                    'datetime' => $datetime,
                    'employee' => $event->cardCode,
                    'status' => $event->direction,
                    'event_id' => $event->id,
                    'object' => $event->object,
                ]);

                if (!$skud->save()) {
                    continue;
                }
            } else {
                continue;
            }
        }
        return 'Save complete';
    }
}