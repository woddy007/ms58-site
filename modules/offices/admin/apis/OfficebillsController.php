<?php

namespace app\modules\offices\admin\apis;

use Yii;
use app\controllers\BaseApiController;
use app\modules\offices\models\Officebills;
use yii\base\Action;

/**
 * Officebills Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class OfficebillsController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Officebills';
    
    public $authOptional = ['searchbydaterange', 'options'];

    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }
    
    public function actionIndex($office_id)
    {
        $bills = Officebills::find()->where(['office_id' => $office_id])->all();
        return $bills;
    }

    public function actionSearchbydaterange ($date_from, $date_to, $office_id)
    {
        $date_from = strtotime($date_from);
        $date_to = strtotime($date_to);
        $bills = Officebills::find()->andWhere(['office_id' => $office_id])->andWhere(['between', 'date', $date_from, $date_to])->all();
        return $bills;
    }
}