<?php

namespace app\modules\offices\admin\apis;

use Yii;
use app\controllers\BaseApiController;
use luya\admin\models\StorageFile;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use luya\admin\events\FileDownloadEvent;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use app\modules\users\models\Owner;
use app\modules\offices\models\Office;
use yii\base\Action;

/**
 * Office Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class OfficeController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\offices\models\Office';
    
    // public function actionPermissions()
    // {
    //     return ['download' => \luya\admin\components\Auth::CAN_VIEW];
    // }
    public $authOptional = ['download', 'getofficeinfo', 'options'];
    public function actionDownload($id, $token)
    {

        // find file in file query
        $fileData = Yii::$app->storage->findFile(['id' => $id, 'is_deleted' => false]);
        // proceed when file exists
        if ($fileData && $fileData->fileExists) {
            // verify again against database to add counter
            $model = StorageFile::findOne($fileData->id);
            // proceed when model exists
            if ($model) {
                $event = new FileDownloadEvent(['file' => $fileData]);
                
                // Yii::$app->trigger(Module::EVENT_BEFORE_FILE_DOWNLOAD, $event);
                
                if (!$event->isValid) {
                    throw new BadRequestHttpException('Unable to perform this file download request due to access restrictions.');
                }
                
                // update the model count stats
                $model->updateCounters(['passthrough_file_stats' => 1]);

                return Yii::$app->response->sendContentAsFile($fileData->content, $model->name_original, [
                    'inline' => (bool) $model->inline_disposition,
                    'mimeType' => $model->mime_type,
                ]);
            }
        }
        
        // throw not found http exception, will not trigger error api transfer.
        throw new NotFoundHttpException("Unable to find requested file with id = '{$id}'.");
    }
    
    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex()
    {
        $owner = Yii::$app->jwt->identity;
        $owner_id = $owner->id;
        $offices = Office::find()->where(['owner' => $owner_id])->all();
        return $offices;
    }
    public function actionGetofficeinfo ()
    {
        $offices = Office::find()->all();
        return $offices;
    }
}