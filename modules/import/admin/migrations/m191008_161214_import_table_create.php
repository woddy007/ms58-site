<?php

use yii\db\Migration;

/**
 * Class m191008_161214_import_table_create
 */
class m191008_161214_import_table_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191008_161214_import_table_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191008_161214_import_table_create cannot be reverted.\n";

        return false;
    }
    */
}
