<?php

namespace app\modules\users\admin;

/**
 * Users Admin Module.
 *
 * File has been created with `module/create` command. 
 * 
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
	    'api-users-owner' => 'app\modules\users\admin\apis\OwnerController',
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Собственники', 'person')
	            ->group('Пользователи')
	                ->itemApi('Собственники', 'usersadmin/owner/index', 'person', 'api-users-owner');
	}
	public function extendPermissionRoutes()
    {
        return [
			['route' => 'usersadmin/owner/me', 'alias' => 'me'],
        ];
    }
}