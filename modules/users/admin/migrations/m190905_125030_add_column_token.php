<?php

use yii\db\Migration;

/**
 * Class m190905_125030_add_column_token
 */
class m190905_125030_add_column_token extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('owner', 'token', $this->string(1024));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('owner', 'token');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190905_125030_add_column_token cannot be reverted.\n";

        return false;
    }
    */
}
