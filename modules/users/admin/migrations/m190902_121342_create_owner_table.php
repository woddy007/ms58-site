<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%owner}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%owner}}`
 * - `{{%office}}`
 */
class m190902_121342_create_owner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%owner}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->unique(),
            'pass' => $this->string()->notNull(),
            'name' => $this->string(),
            'middlename' => $this->string(),
            'lastname' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'status' => $this->integer(),
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        $this->dropTable('{{%owner}}');
    }
}
