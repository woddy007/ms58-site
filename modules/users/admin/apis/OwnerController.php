<?php

namespace app\modules\users\admin\apis;

use app\controllers\BaseApiController;
use app\modules\offices\models\Cards;
use app\modules\offices\models\Office;
use app\modules\offices\models\Pribor;
use app\modules\offices\models\Videocams;
use app\modules\offices\models\VideoConnectOffice;
use app\modules\users\models\Owner;
use Yii;

/**
 * Owner Controller.
 *
 * File has been created with `crud/create` command.
 */
class OwnerController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\users\models\Owner';

    public function actionPermissions()
    {
        return [
            'me' => \luya\admin\components\Auth::CAN_VIEW,
        ];
    }

    public function actionLogin()
    {
        $model = new Owner();
        $model->scenario = Owner::SCENARIO_LOGIN;
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            $user = Owner::find()->where(['login' => $model->login])->one();
            if ($user && $user!==NULL && $user->status == 1) {
                if ($user && $user!==NULL && Yii::$app->security->validatePassword($model->pass, $user->pass)) {
                    if ($user->updateAttributes(['token' => Yii::$app->jwt->generateToken($user)])) {
                        return $user;
                    }

                } else {
                    $model->addError('login', 'Неправельный Логин или Пароль!');
                }
            } else {
                $model->addError('status', 'Доступ запрещен! Обратитесь к администратору!');
            }
        } else {
            return $this->sendModelError($model);
        }


    }

    public function actionMe()
    {
        $user = Yii::$app->jwt->identity;
        $offices = Office::find()->where(['owner' => $user->id])->all();
        foreach ($offices as $office) {
            $offices_data [] = $office->id;
            $offices_names [] = $office->name;
            if ($office->pribor == NULL) {
                continue;
            }
            $pribors [] = $office->pribor;
        }
        $officecams = VideoConnectOffice::find()->where(['in', 'office_id', $offices_data])->all();
        $cams = [];
        foreach ($officecams as $cam){
            $cams [] = $cam->videocam_id;
        }
        $camers = Videocams::find()->where(['in', 'id', $cams])->all();
        $cards = Cards::find()->where(['in', 'office_id', $offices_data])->all();
        $pribor = Pribor::find()->where(['in', 'id', $pribors])->all();
        return ['owner' => $user, 'offices' => $offices_names, 'cams' => $camers, 'cards' => $cards, 'pribors' => $pribor];
    }
}
