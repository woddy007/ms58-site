<?php

namespace app\modules\users\admin\apis;

use app\controllers\BaseApiController;
use app\modules\users\models\Owner;
use luya\admin\base\RestController;
use Yii;
use yii\web\NotFoundHttpException;
/**
 * Owner Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class CustomOwnerController extends RestController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\users\models\Owner';


    public function actionLogin()
    {
        $model = new Owner();
        $model->scenario = Owner::SCENARIO_LOGIN;
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            $user = Owner::find()->where(['login' => $model->login])->one();
            if ($user->status == 1) {
	            if ($user && Yii::$app->security->validatePassword($model->pass, $user->pass)) {
	                if ($user->updateAttributes(['token' => Yii::$app->jwt->generateToken($user)])) {
	                    return $user;
	                }
	        
	            } else {
	                $model->addError('login', 'Неправельный Логин или Пароль!');
	            }
	        } else {
	        	$model->addError('status', 'Доступ запрещен! Обратитесь к администратору!');
	        }
        }

        return $this->sendModelError($model);
    }

    public function actionMe()
    {
        return Yii::$app->jwt->identity;
    }
}