<?php

namespace app\modules\users\models;

use luya\admin\base\JwtIdentityInterface;
use Yii;
use \luya\admin\aws\ChangePasswordInterface;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\aws\ChangePasswordActiveWindow;

/**
 * Owner.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $login
 * @property string $pass
 * @property string $name
 * @property string $middlename
 * @property string $lastname
 * @property integer $phone
 * @property string $email
 * @property integer $status
 */
class Owner extends NgRestModel implements JwtIdentityInterface, ChangePasswordInterface
{
    CONST SCENARIO_LOGIN = 'login';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%owner}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-users-owner';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Логин'),
            'pass' => Yii::t('app', 'Пароль'),
            'name' => Yii::t('app', 'Имя'),
            'middlename' => Yii::t('app', 'Отчество'),
            'lastname' => Yii::t('app', 'Фамилия'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Статус'),
            'token' => Yii::t('app', 'Token'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'status'], 'integer'],
            [['login', 'pass'], 'required'],
            [['login', 'pass', 'name', 'middlename', 'lastname', 'email'], 'string', 'max' => 255],
            [['token'], 'string', 'max' => 1024],
            [['login'], 'unique', 'except' => self::SCENARIO_LOGIN],
            
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_LOGIN] = $scenarios[self::SCENARIO_DEFAULT];
        return $scenarios;
    }

    public function ngRestActiveWindows()
    {
        return [
            ['class' => ChangePasswordActiveWindow::class, 'label' => false],
        ];
    }

    public function changePassword($newpass)
    {
        $this->pass = $newpass;

        if ($this->encodePassword()) {
            if ($this->save()) {
                return true;
            }
        }

        return $this->addError('newpass', 'Error while saving new password.');
    }

    public function encodePassword()
    {
        if (!$this->validate(['pass'])) {
            return false;
        }
        $this->pass = Yii::$app->getSecurity()->generatePasswordHash($this->pass);
        return true;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['pass'], $fields['is_deleted'], $fields['settings']);
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'login' => 'text',
            'pass' => 'password',
            'name' => 'text',
            'middlename' => 'text',
            'lastname' => 'text',
            'phone' => 'text',
            'email' => 'text',
            'status' => 'toggleStatus',
            'token' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['login', 'name', 'middlename', 'lastname', 'phone', 'email', 'status']],
            [['create'], ['pass']],
            [['create', 'update'], ['login', 'name', 'middlename', 'lastname', 'phone', 'email', 'status']],
            ['delete', true],
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public static function loginByJwtToken(\Lcobucci\JWT\Token $token)
    {
        return self::findOne(['token' => $token->__toString()]);
    }

    

    public function beforeSave($insert){
        if ($insert) {
            if(isset($this->pass)) {
                    $this->pass = Yii::$app->getSecurity()->generatePasswordHash($this->pass);}
        
            } 
        return parent::beforeSave($insert);
    }
}