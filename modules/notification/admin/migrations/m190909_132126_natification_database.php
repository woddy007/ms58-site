<?php

use yii\db\Migration;

/**
 * Class m190909_132126_natification_database
 */
class m190909_132126_natification_database extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'datetime' => $this->integer()->notNull(),
            'office_id' => $this->integer()->notNull(),
            'notification' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notification');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190909_132126_natification_database cannot be reverted.\n";

        return false;
    }
    */
}
