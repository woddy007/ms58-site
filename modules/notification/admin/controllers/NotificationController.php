<?php

namespace app\modules\notification\admin\controllers;

/**
 * Notification Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class NotificationController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\notification\models\Notification';
    

}