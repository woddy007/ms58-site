<?php

namespace app\modules\notification\admin;

/**
 * Notification Admin Module.
 *
 * File has been created with `module/create` command. 
 * 
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
	    'api-notification-notification' => 'app\modules\notification\admin\apis\NotificationController',
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Уведомления', 'sms')
	            ->group(' ')
	                ->itemApi('Уведомления', 'notificationadmin/notification/index', 'sms', 'api-notification-notification');
	}

}