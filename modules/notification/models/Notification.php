<?php

namespace app\modules\notification\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;


/**
 * Notification.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $datetime
 * @property integer $office_id
 * @property string $notification
 */
class Notification extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-notification-notification';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'datetime' => Yii::t('app', 'Дата и время'),
            'office_id' => Yii::t('app', 'Офис'),
            'notification' => Yii::t('app', 'Уведомление'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'office_id', 'notification'], 'required'],
            [['datetime', 'office_id'], 'integer'],
            [['notification'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'datetime' => 'datetime',
            'office_id' => [
                'selectModel',
                'modelClass' => \app\modules\offices\models\Office::className(),
                'valueField' => 'id',
                'labelField' => 'name',
            ],
            'notification' => 'textarea',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['datetime', 'office_id', 'notification']],
            [['create', 'update'], ['datetime', 'office_id', 'notification']],
            ['delete', true],
        ];
    }



    // public function afterSave($insert, $changedAttributes)
    // {
    //      parent::afterSave($insert, $changedAttributes);
    //      if($insert)
    //      {
    //          $this->sendNotifications();
    //      }
    // }

}