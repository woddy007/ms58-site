<?php

namespace app\modules\servicerequest\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Servicerequest.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $datetime
 * @property integer $office_id
 * @property string $category
 * @property string $urgency
 * @property string $usercomment
 * @property string $servicecomment
 * @property integer $status
 */
class Servicerequest extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%servicerequest}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-servicerequest-servicerequest';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app','Дата создания'),
            'updated_at' => Yii::t('app', 'Дата изменения'),
            'office_id' => Yii::t('app', 'Офис'),
            'category' => Yii::t('app', 'Категория'),
            'urgency' => Yii::t('app', 'Срочность'),
            'usercomment' => Yii::t('app', 'Комментарий заявителя'),
            'servicecomment' => Yii::t('app', 'Комментарий исполнителя'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id', 'category'], 'required'],
            [['urgency', 'category', 'office_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['usercomment', 'servicecomment'], 'string', 'max' => 255],
        ];
    }



    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
            'office_id' => [
                'selectModel',
                'modelClass' => \app\modules\offices\models\Office::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->company;
                },
            ],
            'category' => ['selectArray', 'data' => self::getCategories()],
            'urgency' => ['selectArray', 'data' => self::getUrgency()],
            'usercomment' => 'textarea',
            'servicecomment' => 'textarea',
            'status' => 'toggleStatus',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['created_at', 'updated_at', 'office_id', 'category', 'urgency', 'usercomment', 'servicecomment', 'status']],
            [['create', 'update'], ['office_id', 'category', 'urgency', 'servicecomment', 'status']],
            ['delete', true],
        ];
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ]);
    }
    public function extraFields()
    {
        return [
            'categoryTitle' => function($model){ 
                return self::getCategories()[$model->category]; },
            'urgencyTitle' => function($model){ 
                return self::getUrgency()[$model->urgency]; }
        ];
    }
    public static function getCategories()
    {
        return [1 => 'Клининг', 2 => 'IT', 3 => 'Сервис'];
    }
    public static function getUrgency()
    {
        return [1 => 'Срочно', 2 => 'Обычно', 3 => 'Не срочно'];
    }
}