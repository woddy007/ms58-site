<?php

namespace app\modules\servicerequest\admin;

/**
 * Servicerequest Admin Module.
 *
 * File has been created with `module/create` command. 
 * 
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
	    'api-servicerequest-servicerequest' => 'app\modules\servicerequest\admin\apis\ServicerequestController',
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Сервисные заявки', 'delete')
	            ->group(' ')
	                ->itemApi('Заявки', 'servicerequestadmin/servicerequest/index', 'class', 'api-servicerequest-servicerequest');
	}

	public function extendPermissionRoutes()
    {
        return [
			['route' => 'officesadmin/office/search', 'alias' => 'search'],
        ];
    }
}