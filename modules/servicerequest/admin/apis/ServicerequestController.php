<?php

namespace app\modules\servicerequest\admin\apis;

use app\controllers\BaseApiController;
use yii\data\Pagination;
use app\modules\servicerequest\models\Servicerequest;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;

/**
 * Servicerequest Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class ServicerequestController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\servicerequest\models\Servicerequest';

    public $pagination = ['defaultPageSize' => 10];
    
    public function actionPermissions()
    {
        return ['search' => \luya\admin\components\Auth::CAN_VIEW,
                'index' => \luya\admin\components\Auth::CAN_VIEW,
        ];
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
        
    }

    public function actionIndex($office_id)
    {
        return new ActiveDataProvider([
            'query' => Servicerequest::find()->where(['office_id' => $office_id])->orderBy(['created_at' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    
    }
    // public function actionSearch($key=NULL)
    // {
    //     if ($key == NULL) {
    //         $provider = Servicerequest::find()->andWhere(['office_id' => $office_id])->all();
    //     }
    //     else 
    //     {
    //         $provider = Servicerequest::find()->andWhere(['office_id' => $office_id])->andWhere(['servicecomment' => $key])->all();
    //     }
    //     return $provider;
    // }
}