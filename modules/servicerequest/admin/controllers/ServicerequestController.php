<?php

namespace app\modules\servicerequest\admin\controllers;

/**
 * Servicerequest Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class ServicerequestController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\servicerequest\models\Servicerequest';
}