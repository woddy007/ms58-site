<?php

use yii\db\Migration;

/**
 * Class m190917_101126_add_column_updated_at
 */
class m190917_101126_add_column_updated_at extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('servicerequest', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('servicerequest', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190917_101126_add_column_updated_at cannot be reverted.\n";

        return false;
    }
    */
}
