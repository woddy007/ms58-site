<?php

use yii\db\Migration;

/**
 * Class m190917_115919_drop_column_datetime
 */
class m190917_115919_drop_column_datetime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('servicerequest', 'datetime');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190917_115919_drop_column_datetime cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190917_115919_drop_column_datetime cannot be reverted.\n";

        return false;
    }
    */
}
