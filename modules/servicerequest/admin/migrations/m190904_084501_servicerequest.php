<?php

use yii\db\Migration;

/**
 * Class m190904_084501_servicerequest
 */
class m190904_084501_servicerequest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('servicerequest', [
            'id' => $this->primaryKey(),
            'datetime' => $this->integer()->notNull(),
            'office_id' => $this->integer()->notNull(),
            'category' => $this->string()->notNull(),
            'urgency' => $this->string(),
            'usercomment' => $this->string(),
            'servicecomment' => $this->string(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('servicerequest');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190904_084501_servicerequest cannot be reverted.\n";

        return false;
    }
    */
}
