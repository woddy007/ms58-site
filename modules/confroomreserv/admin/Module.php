<?php

namespace app\modules\confroomreserv\admin;

/**
 * Confroomreserv Admin Module.
 *
 * File has been created with `module/create` command. 
 * 
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
	    'api-confroomreserv-confreserv' => 'app\modules\confroomreserv\admin\apis\ConfreservController',
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Бронирование конференц зала', 'home_work')
	            ->group(' ')
	                ->itemApi('Бронирования', 'confroomreservadmin/confreserv/index', 'event_note', 'api-confroomreserv-confreserv');
	}

}