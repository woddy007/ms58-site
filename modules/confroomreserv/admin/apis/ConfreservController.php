<?php

namespace app\modules\confroomreserv\admin\apis;

use app\controllers\BaseApiController;
use yii\base\Action;
use app\modules\confroomreserv\models\Confreserv;
use Yii;
use yii\web\NotFoundHttpException;
use  yii\web\YiiAsset;
use yii\filters\VerbFilter;
/**
 * Confreserv Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class ConfreservController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\confroomreserv\models\Confreserv';

    public $pagination = ['defaultPageSize' => 9999, 'pageSizeLimit' => [1, 9999]];

    public $authOptional = ['checkrange', 'options'];
    
    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }
    public function actionIndex()
    {
        $bigdata = [];
        $confreserv = Confreserv::find()->all();
        $statuses = \yii\helpers\ArrayHelper::map(Confreserv::find()->select(new \yii\db\Expression("FROM_UNIXTIME(`date`, '%Y/%m/%d') as `date`, count(id) as count"))->groupBy('date')->asArray()->all(),
        'date', 'count');
        foreach ($confreserv as $conf)
        {
           $date = date('Y/m/d',$conf->date);
           $status = $statuses[$date] < 12 ? '2' : '3';
           $bigdata[$date]['ranges'][] = ['office_id' => $conf->office_id, 'office_name' => $conf->office->name, 'range_id' => $conf->range];
           $bigdata[$date]['status'] = $status;

        }
        return $bigdata;
    }

    public function actionCheckrange() 
    {
        $bigdata = Yii::$app->request->getBodyParams();

        foreach ($bigdata as $data) 
        {
            $date =  $data["date"];
            $range =  $data["range"];
            $office_id = $data["office_id"];
            $date = strtotime($date);
            $checkdate = Confreserv::find()->where(['date' => $date, 'range' => $range])->one();
            if ($checkdate==NULL) {
                $checkdate = new Confreserv();
                $checkdate->date=$date;
                $checkdate->office_id=$office_id;
                $checkdate->range=$range;
                if (!$checkdate->save()) {
                    return "Error" . print_r($checkdate->errors, true);
                }
            } 
        }
    }

    public function actionDelete($date, $range) {
        $date = strtotime($date);
        if ($model = Confreserv::find()->where(['date' => $date, 'range' => $range])->one()) {
            return $model->delete();
        }
        else 
        {   
            $date=date('Y/m/d', $date);
            throw new NotFoundHttpException("Unable to find requested file with date = '{$date}' and range '{$range}'.");
        }
    }

}