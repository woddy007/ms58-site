<?php

namespace app\modules\confroomreserv\admin\controllers;

/**
 * Confreserv Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class ConfreservController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\confroomreserv\models\Confreserv';
}