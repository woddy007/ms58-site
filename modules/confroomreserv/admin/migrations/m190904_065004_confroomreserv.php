<?php

use yii\db\Migration;

/**
 * Class m190904_065004_confroomreserv
 */
class m190904_065004_confroomreserv extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('confroomreserv', [
            'id' => $this->primaryKey(),
            'datetimestart' => $this->integer()->notNull(),
            'datetimeend' => $this->integer()->notNull(),
            'office_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('confroomreserv');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190904_065004_confroomreserv cannot be reverted.\n";

        return false;
    }
    */
}
