<?php

use yii\db\Migration;

/**
 * Class m190911_075105_add_column_status
 */
class m190911_075105_add_column_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('confroomreserv', 'status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('confroomreserv', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190911_075105_add_column_status cannot be reverted.\n";

        return false;
    }
    */
}
