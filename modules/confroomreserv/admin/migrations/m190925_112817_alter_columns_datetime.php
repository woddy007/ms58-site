<?php

use yii\db\Migration;

/**
 * Class m190925_112817_alter_columns_datetime
 */
class m190925_112817_alter_columns_datetime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('confroomreserv', 'datetimestart');
        $this->dropColumn('confroomreserv','datetimeend');
        $this->addColumn('confroomreserv', 'date', $this->integer());
        $this->addColumn('confroomreserv', 'range', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('confroomreserv','datetimestart',  $this->integer());
        $this->addColumn('confroomreserv','datetimeend', $this->integer());
        $this->dropColumn('confroomreserv', 'date');
        $this->dropColumn('confroomreserv', 'range');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190925_112817_alter_columns_datetime cannot be reverted.\n";

        return false;
    }
    */
}
