<?php

namespace app\modules\confroomreserv\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Confreserv.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $datetime
 * @property integer $office_id
 */
class Confreserv extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%confroomreserv}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-confroomreserv-confreserv';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Дата'),
            'range' => Yii::t('app', 'Временной интервал'),
            'office_id' => Yii::t('app', 'Офис'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'office_id'], 'required'],
            [['date', 'office_id', 'status', 'range'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'range' => ['selectArray', 'data' => self::getRange()],
            'date' => 'date',
            'office_id' => [
                'selectModel',
                'modelClass' => \app\modules\offices\models\Office::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    return $model->name . ' ' . $model->company;
                },
            ],
           'status' => ['selectArray', 'data' => self::getStatus()],
        ];
    }

    public function extraFields()
    {
        return [
            'rangeTitle' => function($model){ 
                return self::getRange()[$model->range]; },
            'statusTitle' => function($model){ 
                return self::getStatus()[$model->status]; },
            'office'
        ];
    }
    public function getOffice() {
        return $this->hasOne(\app\modules\offices\models\Office::className(), ['id' => 'office_id'] );
    }

    public static function getRange()
    {
        return [
            1 => '08:00-9:00', 2 => '09:00-10:00', 
            3 => '10:00-11:00', 4 => '11:00-12:00',
            5 => '12:00-13:00', 6 => '13:00-14:00',
            7 => '14:00-15:00', 8 => '15:00-16:00',
            9 => '16:00-17:00', 10 => '17:00-18:00',
            11 => '18:00-19:00', 12 => '19:00-20:00'
        ];
    }
    public static function getStatus()
    {
        return [
            1 => 'Свободен', 2 => 'Частично занят', 
            3 => 'Занят'
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['date', 'range', 'office_id', 'status']],
            [['create', 'update'], ['date', 'range', 'office_id']],
            ['delete', true],
        ];
    }

    // public function beforeSave($insert)
    // {
    //     if (parent::beforeSave($insert)) {
    //         // $this->date = strtotime($this->date);
    //         $range = json_decode($this->range, true);
    //         if (empty($range)) {
    //             return $this->status = 1;
    //         }
    //         elseif (count($range)==12) {
    //             return $this->status = 3;
    //         } 
    //         elseif (count($range) > 0 && count($range) < 12) {
    //             return $this->status = 2;
    //         }
    //         else {
    //             return "Error";
    //         }             
    //     }
    // }
}