<?php

namespace app\modules\bills\admin\apis;

use app\controllers\BaseApiController;

/**
 * Bill Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class BillController extends BaseApiController
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\bills\models\Bill';


}