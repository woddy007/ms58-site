<?php

namespace app\modules\bills\admin;

/**
 * Bills Admin Module.
 *
 * File has been created with `module/create` command. 
 * 
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
    public $apis = [
        'api-bills-bill' => 'app\modules\bills\admin\apis\BillController',
    ];
    
    public function getMenu()
    {
        return (new \luya\admin\components\AdminMenuBuilder($this))
            ->node('Счета', 'monetization_on')
                ->group('Счета')
                    ->itemApi('Счета', 'billsadmin/bill/index', 'monetization_on', 'api-bills-bill');
    }
    
}