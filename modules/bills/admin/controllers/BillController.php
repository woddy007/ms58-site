<?php

namespace app\modules\bills\admin\controllers;

/**
 * Bill Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class BillController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\bills\models\Bill';
}