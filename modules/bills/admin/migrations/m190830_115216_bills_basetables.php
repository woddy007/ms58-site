<?php

use yii\db\Migration;

/**
 * Class m190830_115216_bills_basetables
 */
class m190830_115216_bills_basetables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('office', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
        $this->createTable('bills', [
            'id' => $this->primaryKey(),
            'date' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'file' => $this->string()->notNull(),
            'status' => $this->integer(),
        ]);
        $this->createTable('uchet', [
            'id' => $this->primaryKey(),
            'date' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'indication' => $this->string()->notNull(),
        ]);
        $this->createTable('docs', [
            'id' => $this->primaryKey(),
            'date' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'file' => $this->string()->notNull(),
        ]);
        $this->createTable('skud', [
            'id' => $this->primaryKey(),
            'datetime' => $this->integer()->notNull(),
            'employee' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('office');
        $this->dropTable('bills');
        $this->dropTable('uchet');
        $this->dropTable('docs');
        $this->dropTable('skud');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190830_115216_bills_basetables cannot be reverted.\n";

        return false;
    }
    */
}
