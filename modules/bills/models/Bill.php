<?php

namespace app\modules\bills\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Bill.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property date $date
 * @property string $name
 * @property string $file
 * @property string $status
 */
class Bill extends NgRestModel
{
    /**
     * @inheritdoc
     */
    //public $i18n = ['date', 'name', 'file', 'status'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bills}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-bills-bill';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Дата'),
            'name' => Yii::t('app', 'Наименование'),
            'file' => Yii::t('app', 'Файл счета'),
            'status' => Yii::t('app', 'Оплачен'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'name', 'file', 'status'], 'required'],
            [['status', 'date', 'file'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'office' => [
                'selectModel',
                'modelClass' => Office::className(),
                'valueField' => 'office_id',
                'labelField' => 'name',
            ],
            'date' => 'date',
            'name' => 'text',
            'file' => 'file',
            'status' => 'toggleStatus',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['date', 'name', 'file', 'status']],
            [['create', 'update'], ['date', 'name', 'file', 'status']],
            ['delete', true],
        ];
    }
}